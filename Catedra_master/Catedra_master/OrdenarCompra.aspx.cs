﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class OrdenarCompra : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmd2 = new SqlCommand();
        SqlCommand cmd3 = new SqlCommand();
        SqlCommand cmd4 = new SqlCommand();
        SqlCommand cmd5 = new SqlCommand();
        SqlCommand cmd6 = new SqlCommand();

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);


        int id = 0;
        int idProveedor = 0;
        float precioUnitario = 0;
        int UltimoID = 0;
        int cantidadMax = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Verifica el inicio de sesion
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }

            txtNombre.Enabled = false;
            txtPrecio.Enabled = false;
            id = int.Parse(Request.QueryString["id"]);//obtiene el id del producto a ordenar


            using (conn)
            {
                //Obtiene los datos de bodega
                cmd5.CommandType = CommandType.StoredProcedure;
                cmd5.CommandText = "getBodega";
                cmd5.Connection = conn;
                conn.Open();
                ddlBodega.DataSource = cmd5.ExecuteReader();
                ddlBodega.DataTextField = "Nombre";
                ddlBodega.DataValueField = "id";
                ddlBodega.DataBind();
                conn.Close();

                //Obtiene el producto por ID
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "getProductosID";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                cmd.Connection = conn;
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                txtNombre.Text = Convert.ToString(dr["Nombre"]).TrimEnd();
                txtPrecio.Text = Convert.ToString(dr["Precio de compra"]);
                precioUnitario = float.Parse(Convert.ToString(dr["Precio de compra"]));
                idProveedor = Int32.Parse(Convert.ToString(dr["Proveedor"]));
                conn.Close();
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            using (conn2)
            {
                //Busca el ultimo id de la tabla compra
                cmd3.CommandText = "UltimoIDCompra";
                cmd3.Connection = conn2;
                conn2.Open();
                SqlDataReader dr = cmd3.ExecuteReader();
                dr.Read();
                UltimoID = Int32.Parse(Convert.ToString(dr["Ultimo"]));
                Session["UltimoCompra"] = Int32.Parse(Convert.ToString(dr["Ultimo"]));
                conn2.Close();

                //Suma la cantidad a la tabla productos
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.CommandText = "ActualizarCantidadCompra";
                cmd2.Parameters.Add("@cantidad", SqlDbType.Int).Value = Convert.ToInt32(txtCant.Text);
                cmd2.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cmd2.Connection = conn2;
                conn2.Open();
                cmd2.ExecuteNonQuery();
                conn2.Close();

                //Inserta un nuevo movimiento de entrada
                String actual = DateTime.Now.ToString("yyyy-MM-dd");
                cmd6.CommandType = CommandType.StoredProcedure;
                cmd6.CommandText = "insertarMovimiento";
                cmd6.Parameters.Add("@TMovimiento", SqlDbType.NVarChar).Value = 'E';
                cmd6.Parameters.Add("@IDProducto", SqlDbType.Int).Value = id;
                cmd6.Parameters.Add("@IDUsuario", SqlDbType.Int).Value = (int)Session["id"];
                cmd6.Parameters.Add("@BodegaEntrada", SqlDbType.Int).Value = Int32.Parse(ddlBodega.SelectedItem.Value);
                cmd6.Parameters.Add("@FechaEntrada", SqlDbType.Date).Value = actual;
                cmd6.Parameters.Add("@Cantidad", SqlDbType.Int).Value = Int32.Parse(txtCant.Text);
                cmd6.Connection = conn2;
                conn2.Open();
                cmd6.ExecuteNonQuery();
                conn2.Close();


                //Inserta dato en la tabla detallecompraventa
                cmd4.CommandType = CommandType.StoredProcedure;
                cmd4.CommandText = "InsertarDetalleCompra";
                cmd4.Parameters.Add("@idProducto", SqlDbType.Int).Value = id;
                cmd4.Parameters.Add("@IDProveedor", SqlDbType.Int).Value = idProveedor;
                cmd4.Parameters.Add("@Cantidad", SqlDbType.Int).Value = Convert.ToInt32(txtCant.Text);
                cmd4.Parameters.Add("@PrecioUnitario", SqlDbType.Float).Value = Math.Round(precioUnitario, 2);
                cmd4.Parameters.Add("@IDCompra", SqlDbType.Int).Value = UltimoID;
                cmd4.Connection = conn2;
                conn2.Open();
                cmd4.ExecuteNonQuery();
                conn2.Close();

                Session["cantidad1"] = Int32.Parse(txtCant.Text);
            }
            Response.Redirect("Compra.aspx?a=s");
        }
    }
}