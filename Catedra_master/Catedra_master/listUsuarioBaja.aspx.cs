﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Catedra_master
{
    public partial class listUsuarioBaja : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            if (IsPostBack == false)
            {
                
                //Carga el dropdownlist de proveedores
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getTipoUsuario";
                    cmd.Connection = conn;
                    conn.Open();
                    ddlTipo.DataSource = cmd.ExecuteReader();
                    ddlTipo.DataTextField = "Cargo";
                    ddlTipo.DataValueField = "ID";
                    ddlTipo.DataBind();
                    conn.Close();

                    //obtiene los usuario dados de baja en el gridview
                    cmd.CommandText = "getUsuariosBaja";
                    conn.Open();
                    GridView1.DataSource = cmd.ExecuteReader();
                    GridView1.DataBind();
                    conn.Close();
                }
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Da de baja a productos mediante su campo estado designado como '2'
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");//obtiene fecha actual
            try
            {
                id = int.Parse(GridView1.DataKeys[e.RowIndex].Values["ID"].ToString());
                //Da de baja al producto y actualiza el GridView
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarUsuarioEstado";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 1;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();

                }
            }
            catch (Exception ex)
            {

            }
            Response.Redirect("listUsuarioBaja.aspx");
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de productos obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("editarUsuario.aspx?id={0}", nIdEmpleado));
        }

        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            //Resetea los datos buscados
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "getTipoUsuario";
                cmd.Connection = conn;
                conn.Open();
                ddlTipo.DataSource = cmd.ExecuteReader();
                ddlTipo.DataTextField = "Cargo";
                ddlTipo.DataValueField = "ID";
                ddlTipo.DataBind();
                conn.Close();

                //obtiene los usuario dados de baja en el gridview
                cmd.CommandText = "getUsuariosBaja";
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Busca productos por medio del proveedor
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "BusquedaUsuarios2";
                //obtiene los usuario dados de baja en el gridview
                cmd.Parameters.Add("@Tipo", SqlDbType.Int).Value = Int32.Parse(ddlTipo.SelectedItem.Value);
                cmd.Connection = conn;
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
        }
    }
}