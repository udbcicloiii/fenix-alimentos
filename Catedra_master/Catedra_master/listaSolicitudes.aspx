﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="listaSolicitudes.aspx.cs" Inherits="Catedra_master.listaSolicitudes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
 <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li></li>
      </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <header class="major">
 <h2>Lista de solicitudes</h2>
 <p>FÉNIX Alimentos</p>
 </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row uniform 50%">
     <div class="4u 12u$(xsmall)">
 Tipo
<div class="select-wrapper">
 <asp:DropDownList
 ID="ddlTipo" runat="server">
     <asp:ListItem Selected="True" Value="1">Aprobadas</asp:ListItem>
     <asp:ListItem Value="2">En Espera</asp:ListItem>
 </asp:DropDownList>
 </div>
 </div>
<div class="2u 12u$(xsmall)">
 <br />
 <asp:Button ID="btnBuscar" runat="server"
 Text="Buscar" Width="100%" CssClass="special"
OnClick="btnBuscar_Click" />
 </div>
<asp:GridView ID="GridView1" runat="server" 
    DataKeyNames="Id Examen"
    OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
    OnRowDeleting="GridView1_RowDeleting" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/check_flat_icon.png" HeaderText="Ver" ShowSelectButton="True" SelectImageUrl="~/images/check_flat_icon.png" EditText="Ver">
            <controlstyle width="50px" />
        </asp:CommandField>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/eliminar_icon_flat.png" HeaderText="Eliminar" ShowDeleteButton="True" DeleteImageUrl="~/images/eliminar_icon_flat.png" SelectImageUrl="~/images/eliminar_icon_flat.png">
            <controlstyle width="40px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>
 </div>
</asp:Content>
