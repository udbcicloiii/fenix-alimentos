﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="listInventario.aspx.cs" Inherits="Catedra_master.listInventario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
 <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li runat="server" id="nuevoMovimiento"><a href="nuevoMovimiento.aspx" class="button
special">Nuevo Movimiento</a></li>
     <li runat="server" id="nuevaBodega"><a href="nuevaBodega.aspx" class="button
special">Nueva Bodega</a></li>
      </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <header class="major">
 <h2>Lista de Inventario</h2>
 <p>FÉNIX Alimentos</p>
 </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row uniform 50%">
     <div class="4u 12u$(xsmall)">
 Bodega
<div class="select-wrapper">
 <asp:DropDownList
 ID="ddlBodega" runat="server">
 </asp:DropDownList>
 </div>
 </div>
     <div class="4u 12u$(xsmall)">
 Movimiento
<div class="select-wrapper">
 <asp:DropDownList
 ID="ddlTipoMovimiento" AutoPostBack="true" runat="server">
     <asp:ListItem Selected="True" Value="E">Entrada</asp:ListItem>
     <asp:ListItem Value="S">Salida</asp:ListItem>
 </asp:DropDownList>
 </div>
 </div>
      <div class="4u 12u$(xsmall)">
 Producto
<div class="select-wrapper">
 <asp:DropDownList
 ID="ddlProducto" runat="server">
 </asp:DropDownList>
 </div>
 </div>
<div class="2u 12u$(xsmall)">
 <br />
 <asp:Button ID="btnBuscar" runat="server"
 Text="Buscar" Width="100%" CssClass="special"
OnClick="btnBuscar_Click" />
 </div>
         <div class="2u 12u$(xsmall)">
             <br />
             <asp:Button ID="BtnLimpiar" runat="server"
 Text="Limpiar" Width="100%" CssClass="special" OnClick="BtnLimpiar_Click"/>
             </div>
<asp:GridView ID="GridView1" runat="server" 
    DataKeyNames="Id"
    OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
    OnRowDeleting="GridView1_RowDeleting" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
     </asp:GridView>
 </div>
</asp:Content>
