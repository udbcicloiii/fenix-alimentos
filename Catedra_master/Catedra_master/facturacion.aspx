﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="facturacion.aspx.cs" Inherits="Catedra_master.facturacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
 <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li><a href="listaSolicitudes.aspx" class="button
special">Lista solicitudes</a></li>
      </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <header class="major">
 <h2>Factura de "Cliente"</h2>
 <p>FÉNIX Alimentos</p>
 </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row uniform 50%">
<asp:GridView ID="GridView1" runat="server" 
    DataKeyNames="ID,Montototal">
     </asp:GridView>
     <linea></linea>
     <div class="2u 12u$(xsmall)">
 <asp:Button ID="btnAprobar" runat="server"
 Text="Imprimir" Width="100%" CssClass="special"
OnClick="btnAprobar_Click" />
 </div>
 </div>
</asp:Content>
