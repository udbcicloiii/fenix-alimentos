﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class Venta : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        string a;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            if (IsPostBack == false)
            {
                //obtiene los productos dados de alta en el gridview
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conn;

                    //obtiene los productos dados de alta en el gridview
                    cmd.CommandText = "getProductosVentas";
                    conn.Open();
                    GridView1.DataSource = cmd.ExecuteReader();
                    GridView1.DataBind();
                    conn.Close();

                }
                a = (Request.QueryString["a"]);
                if (a == "s")
                {
                    BtnLimpiar.Enabled = false;
                }
                else
                {
                    //Inhabilita el boton de compras 
                    foreach (GridViewRow row in GridView1.Rows)
                    {
                        DataControlFieldCell editable = (DataControlFieldCell)row.Controls[0];
                        editable.Enabled = false;
                    }
                }
            }
        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //obtiene el id de proveedor
            int idProducto = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Value);
            Session["proveedor"] = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Values["IDProveedor"].ToString());
            Response.Redirect(string.Format("OrdenarVenta.aspx?id={0}", idProducto));
        }

        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            BtnLimpiar.Enabled = false;
            //Habilita los botones de compras
            foreach (GridViewRow row in GridView1.Rows)
            {
                DataControlFieldCell editable = (DataControlFieldCell)row.Controls[0];
                editable.Enabled = true;
            }
            //Insertar una nueva compra
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "InsertarVenta";
                cmd.Parameters.Add("@Aprobacion", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@IDCliente",SqlDbType.Int).Value = Session["idCliente"];
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

        }
    }
}