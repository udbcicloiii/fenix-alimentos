﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class LoginCliente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Cookies.Clear();
                }
                catch (Exception error)
                {
                    System.Diagnostics.Debug.WriteLine(error);
                }
            }

        }
        Conexion miConexion = new Conexion();
        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            miConexion.conectar();
            //Verificando si el usuario ingresó las credenciales correctas
            if (miConexion.logInCliente(txtEmail1.Text, txtPass.Text))
            {
                //Si el usuario existe, se redirecciona a la página principal y obtiene el id del usuario logueado
                DataTable dt = miConexion.getUsuarioCliente(txtEmail1.Text);
                DataRow row = dt.Rows[0];
                Session["user"] = txtEmail1.Text;
                Session["idCliente"] = Convert.ToInt32(row["id"]);
                Session["TipoUsuario"] = "10";
                Response.Redirect("menu.aspx");
            }
            else
            {
                //Si no, se le informa al usuario
                lblResultado.Text = "Credenciales incorrectas";
                lblResultado.Visible = true;
            }

            miConexion.desconectar();

        }
    }
}