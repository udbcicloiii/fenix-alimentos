﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="editarProducto.aspx.cs" Inherits="Catedra_master.editarProducto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <!-- Header -->
    <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li><a href="listProducto.aspx" class="button
special">Listado de productos</a></li>
 </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <h3>Editar Productos</h3>
 <p>
 Rellene los siguientes campos
 </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row uniform 50%">
 <div class="6u 12u$(xsmall)">
 <label>Nombre</label>
 <asp:TextBox ID="txtNombre"
 runat="server"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator1"
ControlToValidate="txtNombre" ForeColor="Red"
 runat="server" ErrorMessage="Nombre obligatorio"></asp:RequiredFieldValidator>
      <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator4" runat="server" ErrorMessage="Formato Incorrecto. Ingrese de nuevo" ControlToValidate="txtNombre" ValidationExpression="^([ñA-Za-zñÑáéíóúÁÉÍÓÚ ]*[ñA-Za-zñÑáéíóúÁÉÍÓÚ][ñA-Za-zñÑáéíóúÁÉÍÓÚ ])" ForeColor="#ff490d"></asp:RegularExpressionValidator>

 </div>
 <div class="6u 12u$(xsmall)">
 <label>Proveedor actual</label>
 <div class="select-wrapper">
 <asp:DropDownList
 ID="ddlProveedor" runat="server">
 </asp:DropDownList>
 </div>
 </div>
        </div>
<div class="row uniform 50%">
    <div class="6u 12u$(xsmall)">
 <label>Proveedor a cambiar</label>
 <div class="select-wrapper">
 <asp:DropDownList
 ID="DropDownList1" runat="server">
 </asp:DropDownList>
 </div>
 </div>
    <div class="6u 12u$(xsmall)">
 <label>Cantidad en inventario</label>
 <asp:TextBox ID="txtCant"
 runat="server" placeholder="0.00"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator4"
ControlToValidate="txtCant" ForeColor="Red"
 runat="server" ErrorMessage="Cantidad obligatoria"></asp:RequiredFieldValidator>
 <asp:RegularExpressionValidator Display="Dynamic"
 ID="RegularExpressionValidator3" runat="server" ErrorMessage="Ingrese una cantidad correcta"
 ControlToValidate="txtCant" ValidationExpression="^[0-9]+$" ForeColor="#ff490d"></asp:RegularExpressionValidator>
 </div>
    <div class="6u 12u$(xsmall)">
 <label>Precio de compra</label>
 <asp:TextBox ID="txtPrecioCompra"
 runat="server" placeholder="0.00"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator2"
ControlToValidate="txtPrecioCompra" ForeColor="Red"
 runat="server" ErrorMessage="Precio compra obligatorio"></asp:RequiredFieldValidator>
 <asp:RegularExpressionValidator Display="Dynamic"
 ID="RegularExpressionValidator1" runat="server" ErrorMessage="Ingrese un precio correcto"
 ControlToValidate="txtPrecioCompra" ValidationExpression="^[0-9]+([.][0-9]+)?$" ForeColor="#ff490d"></asp:RegularExpressionValidator>
 </div>   
    <div class="6u 12u$(xsmall)">
 <label>Precio de venta</label>
 <asp:TextBox ID="txtPrecioVenta"
 runat="server" placeholder="0.00"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator3"
ControlToValidate="txtPrecioVenta" ForeColor="Red"
 runat="server" ErrorMessage="Precio venta obligatorio"></asp:RequiredFieldValidator>
 <asp:RegularExpressionValidator Display="Dynamic"
 ID="RegularExpressionValidator2" runat="server" ErrorMessage="Ingrese un precio correcto"
 ControlToValidate="txtPrecioVenta" ValidationExpression="^[0-9]+([.][0-9]+)?$" ForeColor="#ff490d"></asp:RegularExpressionValidator>
 </div>
    <div class="6u 12u$(xsmall)">
        <asp:Label  id="lblerror" runat="server" ForeColor="#ff490d"></asp:Label>
       
    </div>
    </div>
    
      <div class="6u 12u$(xsmall)">
 <asp:Button ID="btnBuscar" runat="server"
 Text="Registrar producto" CssClass="special"
OnClick="btnBuscar_Click" />
 </div>
</asp:Content>
