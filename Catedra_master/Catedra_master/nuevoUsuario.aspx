﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="nuevoUsuario.aspx.cs" Inherits="Catedra_master.nuevoUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
    <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li><a href="listUsuario.aspx" class="button
special">Listado de Usuarios</a></li>
 </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <h3>Añadir Usuario</h3>
 <p>
 Rellene los siguientes campos
 </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row uniform 50%">
 <div class="6u 12u$(xsmall)">
 <label>Usuario</label>
 <asp:TextBox ID="txtUsuario"
 runat="server"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator1"
ControlToValidate="txtUsuario" ForeColor="Red"
 runat="server" ErrorMessage="Nombre obligatorio"></asp:RequiredFieldValidator>
      <asp:RegularExpressionValidator  runat="server"  ControlToValidate="txtUsuario"
            ErrorMessage="Formato de entrada incorrecto" ValidationExpression="^[a-zA-Z]{1,20}$" ForeColor="#ff490d"></asp:RegularExpressionValidator>
 </div>
      <div class="6u 12u$(xsmall)">
 <label>Contraseña</label>
 <asp:TextBox ID="txtContraseña"
 runat="server" TextMode="Password"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator5"
ControlToValidate="txtContraseña" ForeColor="Red"
 runat="server" ErrorMessage="Contraseña obligatoria"></asp:RequiredFieldValidator>
           <asp:RegularExpressionValidator  runat="server"  ControlToValidate="txtContraseña"
            ErrorMessage="Formato de entrada incorrecto" ValidationExpression="^[A-Za-z0-9]{1,20}$" ForeColor="#ff490d"></asp:RegularExpressionValidator>
 </div>
         <div class="6u 12u$(xsmall)">
 <label>Tipo de usuario</label>
 <div class="select-wrapper">
 <asp:DropDownList
 ID="ddlTipoUsuario" AutoPostBack="true" runat="server">
 </asp:DropDownList>
 </div>
 </div>
      <div class="6u 12u$(xsmall)">
          <br />
 <asp:Button ID="btnBuscar" runat="server"
 Text="Registrar Usuario" CssClass="special"
OnClick="btnBuscar_Click" />
 </div>
 </div>
</asp:Content>
