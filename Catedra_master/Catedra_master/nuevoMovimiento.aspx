﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="nuevoMovimiento.aspx.cs" Inherits="Catedra_master.nuevoMovimiento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
    <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li><a href="listInventario.aspx" class="button
special">Listado de movimientos</a></li>
 </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <h3>Añadir Movimientos</h3>
 <p>
 Rellene los siguientes campos
 </p>
      <asp:Label ID="lblResultado" runat="server" ForeColor="#ffff00" Text=""></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
     <div class="row uniform 50%">
      <div class="6u 12u$(xsmall)">
 <label>Producto</label>
 <div class="select-wrapper">
 <asp:DropDownList
 ID="ddlProducto" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlProducto_SelectedIndexChanged">
 </asp:DropDownList>
 </div>
 </div>
         <div class="6u 12u$(xsmall)">
 <label>Cantidad a solicitar</label>
 <asp:TextBox ID="txtCant"
 runat="server"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator4"
ControlToValidate="txtCant" ForeColor="Red"
 runat="server" ErrorMessage="Cantidad obligatoria"></asp:RequiredFieldValidator>
 <asp:RegularExpressionValidator Display="Dynamic"
 ID="RegularExpressionValidator3" runat="server" ErrorMessage="Ingrese una cantidad correcta"
 ControlToValidate="txtCant" ValidationExpression="^[0-9]+$" ForeColor="#ff490d"></asp:RegularExpressionValidator>
 </div>

         <div class="6u 12u$(xsmall)">
 <label>Cantidad disponible en inventario</label>
 <asp:TextBox ID="TextBox1"
 runat="server" ReadOnly="True"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator1"
ControlToValidate="TextBox1" ForeColor="Red"
 runat="server" ErrorMessage="Cantidad obligatoria"></asp:RequiredFieldValidator>
 <asp:RegularExpressionValidator Display="Dynamic"
 ID="RegularExpressionValidator1" runat="server" ErrorMessage="Ingrese una cantidad correcta"
 ControlToValidate="TextBox1" ValidationExpression="^[0-9]+$" ForeColor="#ff490d"></asp:RegularExpressionValidator>
 </div>
<div class="6u 12u$(xsmall)">
 <label>Bodega</label>
 <div class="select-wrapper">
 <asp:DropDownList
 ID="ddlBodega" runat="server">
 </asp:DropDownList>
 </div>
 </div>
     <div class="6u 12u$(xsmall)">
 <label>Tipo de movimiento</label>
 <div class="select-wrapper">
 <asp:DropDownList
 ID="ddlTipoMovimiento" AutoPostBack="true" runat="server">
     <asp:ListItem Selected="True" Value="E">Entrada</asp:ListItem>
     <asp:ListItem Value="S">Salida</asp:ListItem>
 </asp:DropDownList>
 </div>
 </div>

      <div class="6u 12u$(xsmall)">
 <asp:Button ID="btnBuscar" runat="server"
 Text="Registrar movimiento" CssClass="special"
OnClick="btnBuscar_Click" /><br />
          <br />
          <asp:Label ID="Label1" runat="server" ForeColor="#ff490d" ></asp:Label>
 </div>
 </div>
</asp:Content>
