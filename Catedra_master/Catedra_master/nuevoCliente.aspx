﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="nuevoCliente.aspx.cs" Inherits="Catedra_master.nuevoCliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <!-- Header -->
    <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li><a href="listCliente.aspx" class="button
special">Listado de clientes</a></li>
 </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <h3>Añadir Clientes</h3>
 <p>
 Rellene los siguientes campos
 </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row uniform 50%">
 <div class="6u 12u$(xsmall)">
 <label>Nombre</label>
 <asp:TextBox ID="txtNombre"
 runat="server"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator1"
ControlToValidate="txtNombre" ForeColor="Red"
 runat="server" ErrorMessage="Nombre obligatorio"></asp:RequiredFieldValidator>
     <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato Incorrecto. Ingrese de nuevo" ControlToValidate="txtNombre" ValidationExpression="^([ñA-Za-zñÑáéíóúÁÉÍÓÚ ]*[ñA-Za-zñÑáéíóúÁÉÍÓÚ][ñA-Za-zñÑáéíóúÁÉÍÓÚ ])" ForeColor="#ff490d"></asp:RegularExpressionValidator>

 </div>
 <div class="6u 12u$(xsmall)">
 <label>Tipo</label>
 <div class="select-wrapper">
 <asp:DropDownList
 ID="ddlTipo" runat="server">
     <asp:ListItem Selected="True" Value="1">Persona jurídica</asp:ListItem>
     <asp:ListItem Value="2">Persona natural</asp:ListItem>
 </asp:DropDownList>
 </div>
 </div>
         <div class="6u 12u$(xsmall)">
                            <asp:TextBox ID="txtPass" runat="server"
                                placeholder="Contraseña" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ErrorMessage="Contraseña es Requerido"
                                ControlToValidate="txtPass"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator  runat="server"  ControlToValidate="txtPass"
            ErrorMessage="Formato de entrada incorrecto" ValidationExpression="^[A-Za-z0-9]{1,20}$" ForeColor="#ff490d"></asp:RegularExpressionValidator>
                        </div>
      <div class="6u 12u$(xsmall)">
 <asp:Button ID="btnBuscar" runat="server"
 Text="Registrar Cliente" CssClass="special"
OnClick="btnBuscar_Click" />
 </div>
 </div>
</asp:Content>
