﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Catedra_master
{
    public partial class listCliente : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            try
            {
                //Validando que se haya iniciado sesión
                //Escondiendo todas las opciones
                nuevoCliente.Visible = false;
                listClienteBaja.Visible = false;
                SolicitudCliente.Visible = false;
                //Obteniendo las variables de sesión
                string idRol = Session["TipoUsuario"].ToString();
                //Mostrando las opciones acorde al rol
                if (idRol == "1")//Administradores
                {
                    nuevoCliente.Visible = true;
                    listClienteBaja.Visible = true;

                    //Cargando los datos de empleados al gridview
                    if (IsPostBack == false)
                    {
                        //carga la gridview con los datos de clientes

                        using (conn)
                        {
                            //obtiene los clientes dados de alta en el gridview
                            cmd.CommandText = "getClientesAlta";
                            cmd.Connection = conn;
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();

                            //obtiene los clientes dados de alta en el gridview
                            cmd.CommandText = "getClientesAltaSolicitud";
                            cmd.Connection = conn;
                            conn.Open();
                            GridView2.DataSource = cmd.ExecuteReader();
                            GridView2.DataBind();
                            conn.Close();
                        }
                    }
                }
                else if (idRol == "4")//Jefes de Relaciones Exteriores
                {
                    nuevoCliente.Visible = true;
                    listClienteBaja.Visible = true;

                    //Cargando los datos de empleados al gridview
                    if (IsPostBack == false)
                    {
                        //carga la gridview con los datos de clientes

                        using (conn)
                        {
                            //obtiene los clientes dados de alta en el gridview
                            cmd.CommandText = "getClientesAlta";
                            cmd.Connection = conn;
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();

                            //obtiene los clientes dados de alta en el gridview
                            cmd.CommandText = "getClientesAltaSolicitud";
                            cmd.Connection = conn;
                            conn.Open();
                            GridView2.DataSource = cmd.ExecuteReader();
                            GridView2.DataBind();
                            conn.Close();
                        }
                    }
                }
                else if (idRol == "8")//Usuarios de relaciones exteriores
                {
                    
                    GridView1.Columns[1].Visible = false;
                    SolicitudCliente.Visible = true;
                    Button1.Visible = false;
                    Button2.Visible = false;
                    combo.Visible = false;
                    titulosolicitud.Visible = false;
                    //Cargando los datos de empleados al gridview
                    if (IsPostBack == false)
                    {
                        //carga la gridview con los datos de clientes

                        using (conn)
                        {
                            //obtiene los clientes dados de alta en el gridview
                            cmd.CommandText = "getClientesAlta";
                            cmd.Connection = conn;
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();
                        }
                    }

                }
            }
            catch (Exception error)
            {
                Response.Redirect("menu.aspx");
            }


        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Busca los cliente por el tipo de persona(Natural o judicial)
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "BusquedaCliente";
                //obtiene los usuario dados de baja en el gridview
                cmd.Parameters.Add("@Tipo", SqlDbType.Int).Value = Int32.Parse(ddlTipo.SelectedItem.Value);
                cmd.Connection = conn;
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
        }
        protected void btnBuscar_Click2(object sender, EventArgs e)
        {
            //Busca los cliente por el tipo de persona(Natural o judicial)
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "BusquedaClienteSolicitud";
                //obtiene los usuario dados de baja en el gridview
                cmd.Parameters.Add("@Tipo", SqlDbType.Int).Value = Int32.Parse(ddlTipo2.SelectedItem.Value);
                cmd.Connection = conn;
                conn.Open();
                GridView2.DataSource = cmd.ExecuteReader();
                GridView2.DataBind();
                conn.Close();
            }
        }
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Dar de baja a clientes y los envia a otra vista, se le asigna el estado de '2' con el significado de que
            //se encuentra dado de baja
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");//Fecha Actual
            try
            {
                //Dando de baja a cliente seleccionado y actualizando el GridView
                id = int.Parse(GridView1.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarClienteEstado";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 2;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    Response.Redirect("listCliente.aspx");
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de cliente obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("editarCliente.aspx?id={0}", nIdEmpleado));
        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Dar de baja a clientes y los envia a otra vista, se le asigna el estado de '2' con el significado de que
            //se encuentra dado de baja
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");//Fecha Actual
            try
            {
                //Dando de baja a cliente seleccionado y actualizando el GridView
                id = int.Parse(GridView2.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarClienteEstado";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 2;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    Response.Redirect("listCliente.aspx");
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void GridView2_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de cliente obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView2.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("AprobarSolicitudCliente.aspx?id={0}", nIdEmpleado));
        }

        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            //Limpia los datos buscados por el dropdownlist
            using (conn)
            {
                //obtiene los clientes dados de alta en el gridview
                cmd.CommandText = "getClientesAlta";
                cmd.Connection = conn;
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
        }
        protected void BtnLimpiar_Click2(object sender, EventArgs e)
        {
            //Limpia los datos buscados por el dropdownlist
            using (conn)
            {
                //obtiene los clientes dados de alta en el gridview
                cmd.CommandText = "getClientesAltaSolicitud";
                cmd.Connection = conn;
                conn.Open();
                GridView2.DataSource = cmd.ExecuteReader();
                GridView2.DataBind();
                conn.Close();
            }
        }
    }
}