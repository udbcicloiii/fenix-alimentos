﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class VerCarrito2 : System.Web.UI.Page
    {
        //Instancia con la bdd
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmd2 = new SqlCommand();
        SqlCommand cmd3 = new SqlCommand();
        SqlCommand cmd4 = new SqlCommand();
        SqlCommand cmd5 = new SqlCommand();
        SqlCommand cmd6 = new SqlCommand();

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn3 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn4 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        int id = 0;
        String total1 = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }

            //Muestra los productos escogidos por el usuario
            using (conn)
            {
                cmd.CommandText = "VerCarrito2";
                cmd.Connection = conn;
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
            total();
            //Si no hay productos mostrara un mensaje si no mostrara el total
            if (total1 == "")
            {
                Label1.Text = "Por favor ingrese productos al Carrito";
            }
            else
            {
                Label1.Text = "Total: $ " + total1;
            }
        }

        public void total()
        {

            using (conn2)
            {
                //Obtiene el monto total de los productos
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.CommandText = "ObtenerTotal2";
                cmd2.Connection = conn2;
                conn2.Open();
                SqlDataReader dr = cmd2.ExecuteReader();
                dr.Read();
                total1 = Convert.ToString(dr["Precio"]);
                conn2.Close();
            }

        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            //Eliminar un producto del Carrito
            try
            {
                int id2 = int.Parse(GridView1.DataKeys[e.RowIndex].Values["ID"].ToString());
                int idProducto = int.Parse(GridView1.DataKeys[e.RowIndex].Values["idProducto"].ToString());
                int cantidad = int.Parse(GridView1.DataKeys[e.RowIndex].Values["Cantidad"].ToString());
                //System.Web.HttpContext.Current.Response.Write("<script>alert("+id+")</script>");
                using (conn3)
                {
                    cmd6.CommandType = CommandType.StoredProcedure;
                    cmd6.CommandText = "CantidadProductoRetornar";
                    cmd6.Parameters.Add("@id", SqlDbType.Int).Value = idProducto;
                    cmd6.Parameters.Add("@Retorno", SqlDbType.Int).Value = cantidad;
                    cmd6.Connection = conn3;
                    conn3.Open();
                    cmd6.ExecuteNonQuery();
                    conn3.Close();

                    //Elimina productos del carrito
                    cmd3.CommandType = CommandType.StoredProcedure;
                    cmd3.CommandText = "EliminarProductoCarrito";
                    cmd3.Parameters.Add("@id", SqlDbType.Int).Value = id2;
                    cmd3.Connection = conn3;
                    conn3.Open();
                    cmd3.ExecuteNonQuery();
                    conn3.Close();


                }
                Response.Redirect("VerCarrito2.aspx");
            }
            catch (SqlException ex)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert(" + ex + ")</script>");
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            using (conn4)
            {
                //Actualiza el estado para dirigirse al apartado de solicitudes
                cmd4.CommandType = CommandType.StoredProcedure;
                cmd4.CommandText = "ActualizarEstadoVenta";
                cmd4.Parameters.Add("@aprob", SqlDbType.Int).Value = 0;
                cmd4.Parameters.Add("@id", SqlDbType.Int).Value = Session["ultimoVenta"];
                cmd4.Parameters.Add("@Proveedor", SqlDbType.Int).Value = Session["proveedor"];
                cmd4.Connection = conn4;
                conn4.Open();
                cmd4.ExecuteNonQuery();
                conn4.Close();

                
                cmd5.CommandType = CommandType.StoredProcedure;
                cmd5.CommandText = "InsertarMontoTotal2";
                cmd5.Parameters.Add("@monto", SqlDbType.Decimal).Value = total1;
                cmd5.Parameters.Add("@id", SqlDbType.Int).Value = Session["ultimoVenta"];
                cmd5.Connection = conn4;
                conn4.Open();
                cmd5.ExecuteNonQuery();
                conn4.Close();
            }
            Response.Redirect("Venta.aspx");
        }
    }
}