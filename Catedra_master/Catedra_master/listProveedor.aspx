﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="listProveedor.aspx.cs" Inherits="Catedra_master.listProveedor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
 <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li runat="server" id="nuevoProveedor"><a href="nuevoProveedor.aspx" class="button
special">Nuevo Proveedor</a></li>
     <li runat="server" id="listProveedorBaja"><a href="listProveedorBaja.aspx" class="button
special">Proveedores dados de baja</a></li>
       <li runat="server" id="NuevaSolicitudProveedor"><a href="NuevaSolicitudProveedor.aspx" class="button
special">Solicitud de proveedor</a></li>
      </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <header class="major">
 <h2>Lista de proveedores</h2>
 <p>FÉNIX Alimentos</p>
 </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row uniform 50%">

<asp:GridView ID="GridView1" runat="server" 
    DataKeyNames="ID"
    OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
    OnRowDeleting="GridView1_RowDeleting" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/edit_flat_icon.png" HeaderText="Editar" ShowSelectButton="True" SelectImageUrl="~/images/edit_flat_icon.png">
            <controlstyle width="50px" />
        </asp:CommandField>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/eliminar_icon_flat.png" HeaderText="Dar de baja" ShowDeleteButton="True" DeleteImageUrl="~/images/eliminar_icon_flat.png" SelectImageUrl="~/images/eliminar_icon_flat.png">
            <controlstyle width="40px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>
 </div>
     <br />
    <div runat="server" id="titulosolicitud">
          <h2 style="text-align:center">Lista de solicitudes de proveedores</h2>
    </div>
    <br />
     <div class="row uniform 50%">

<asp:GridView ID="GridView2" runat="server" 
    DataKeyNames="ID"
    OnSelectedIndexChanging="GridView2_SelectedIndexChanging"
    OnRowDeleting="GridView2_RowDeleting" OnSelectedIndexChanged="GridView2_SelectedIndexChanged">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/check.png" HeaderText="Aprobar" ShowSelectButton="True" SelectImageUrl="~/images/check.png">
            <controlstyle width="50px" />
        </asp:CommandField>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/eliminar_icon_flat.png" HeaderText="Dar de baja" ShowDeleteButton="True" DeleteImageUrl="~/images/eliminar_icon_flat.png" SelectImageUrl="~/images/eliminar_icon_flat.png">
            <controlstyle width="40px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>
 </div>
</asp:Content>
