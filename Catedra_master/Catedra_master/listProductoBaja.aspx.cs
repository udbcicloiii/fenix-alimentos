﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Catedra_master
{
    public partial class listProductoBaja : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            //Cargando los datos de empleados al gridview
            if (IsPostBack == false)
            {
                //Llena el dropdownlist de proveedor y el gridview de productos dados de baja
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getProveedor";
                    cmd.Connection = conn;
                    conn.Open();
                    ddlProveedor.DataSource = cmd.ExecuteReader();
                    ddlProveedor.DataTextField = "Nombre";
                    ddlProveedor.DataValueField = "id";
                    ddlProveedor.DataBind();
                    conn.Close();

                    
                    //obtiene los productos dados de baja en el gridview
                    cmd.CommandText = "getProductosBaja";
                    conn.Open();
                    GridView1.DataSource = cmd.ExecuteReader();
                    GridView1.DataBind();
                    conn.Close();

                    //obtiene los productos dados de baja en el gridview
                    cmd.CommandText = "getSolicitudProductosBaja";
                    conn.Open();
                    GridView2.DataSource = cmd.ExecuteReader();
                    GridView2.DataBind();
                    conn.Close();

                    //Solicitudes dadas de baja
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getProveedor";
                    cmd.Connection = conn;
                    conn.Open();
                    ddlProveedor2.DataSource = cmd.ExecuteReader();
                    ddlProveedor2.DataTextField = "Nombre";
                    ddlProveedor2.DataValueField = "id";
                    ddlProveedor2.DataBind();
                    conn.Close();
                }
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Da de alta al producto y obtiene la fecha actual
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");
            try
            {
                //Eliminando el cargo seleccionado y actualizando el GridView
                id = int.Parse(GridView1.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarProductoEstado";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 1;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                Response.Redirect("listProductoBaja.aspx");
            }
            catch (Exception ex)
            {

            }

        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de productos obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("editarProducto.aspx?id={0}", nIdEmpleado));
        }


        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Da de alta al producto y obtiene la fecha actual
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");
            try
            {
                //Eliminando el cargo seleccionado y actualizando el GridView
                id = int.Parse(GridView2.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarProductoEstado";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                Response.Redirect("listProductoBaja.aspx");
            }
            catch (Exception ex)
            {

            }

        }

        protected void GridView2_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de productos obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView2.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("editarProducto.aspx?id={0}", nIdEmpleado));
        }



        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Busca productos por el proveedor
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "BusquedaProducto2";
                //obtiene los usuario dados de alta en el gridview
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = Int32.Parse(ddlProveedor.SelectedItem.Value);
                cmd.Connection = conn;
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
        }

        protected void btnBuscar_Click2(object sender, EventArgs e)
        {
            //Busca productos por el proveedor
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "BusquedaProducto2";
                //obtiene los usuario dados de alta en el gridview
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = Int32.Parse(ddlProveedor2.SelectedItem.Value);
                cmd.Connection = conn;
                conn.Open();
                GridView2.DataSource = cmd.ExecuteReader();
                GridView2.DataBind();
                conn.Close();
            }
        }

        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            //Resetea los datos buscados 
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "getProveedor";
                cmd.Connection = conn;
                conn.Open();
                ddlProveedor.DataSource = cmd.ExecuteReader();
                ddlProveedor.DataTextField = "Nombre";
                ddlProveedor.DataValueField = "id";
                ddlProveedor.DataBind();
                conn.Close();

                //obtiene los usuario dados de baja en el gridview
                cmd.CommandText = "getProductosBaja";
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
        }
        protected void BtnLimpiar_Click2(object sender, EventArgs e)
        {
            //Resetea los datos buscados 
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "getProveedor";
                cmd.Connection = conn;
                conn.Open();
                ddlProveedor2.DataSource = cmd.ExecuteReader();
                ddlProveedor2.DataTextField = "Nombre";
                ddlProveedor2.DataValueField = "id";
                ddlProveedor2.DataBind();
                conn.Close();

                //obtiene los usuario dados de baja en el gridview
                cmd.CommandText = "getSolicitudProductosBaja";
                conn.Open();
                GridView2.DataSource = cmd.ExecuteReader();
                GridView2.DataBind();
                conn.Close();
            }
        }

    }
}