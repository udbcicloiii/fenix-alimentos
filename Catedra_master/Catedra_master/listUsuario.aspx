﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="listUsuario.aspx.cs" Inherits="Catedra_master.listUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
   <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li><a href="nuevoUsuario.aspx" class="button
special">Nuevo Usuario</a></li>
     <li><a href="listUsuarioBaja.aspx" class="button
special">Usuarios dados de baja</a></li>
      </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="titulo" runat="server">
    <header class="major">
 <h2>Lista de Usuarios</h2>
 <p>FÉNIX Alimentos</p>
 </header>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row uniform 50%">
     <div class="4u 12u$(xsmall)">
 Tipo de usuario
<div class="select-wrapper">
 <asp:DropDownList
 ID="ddlTipo" runat="server">
 </asp:DropDownList>
 </div>
 </div>
<div class="2u 12u$(xsmall)">
 <br />
 <asp:Button ID="btnBuscar" runat="server"
 Text="Buscar" Width="100%" CssClass="special"
OnClick="btnBuscar_Click" />
 </div>
        <div class="2u 12u$(xsmall)">
             <br />
             <asp:Button ID="BtnLimpiar" runat="server"
 Text="Limpiar" Width="100%" CssClass="special" OnClick="BtnLimpiar_Click"/>
             </div>
<asp:GridView ID="GridView1" runat="server" 
    DataKeyNames="ID"
    OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
    OnRowDeleting="GridView1_RowDeleting" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/edit_flat_icon.png" HeaderText="Editar" ShowSelectButton="True" SelectImageUrl="~/images/edit_flat_icon.png">
            <controlstyle width="50px" />
        </asp:CommandField>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/eliminar_icon_flat.png" HeaderText="Dar de baja" ShowDeleteButton="True" DeleteImageUrl="~/images/eliminar_icon_flat.png" SelectImageUrl="~/images/eliminar_icon_flat.png">
            <controlstyle width="40px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>
 </div>
</asp:Content>
