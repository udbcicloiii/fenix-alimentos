﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Catedra_master
{
    public partial class editarCliente : System.Web.UI.Page
    {
        //Instancia con la BDD
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        int id = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            id = int.Parse(Request.QueryString["id"]);//Obtiene el id del usuario
            //carga los datos al textbox
            if (!Page.IsPostBack)
            {
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getClientes";
                    cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dr.Read();
                    txtNombre.Text = Convert.ToString(dr["Nombre"]).TrimEnd();
                    conn.Close();
                }
            }
        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Obtiene la fecha actual y modifica el cliente
            String modificar = DateTime.Now.ToString("yyyy-MM-dd");
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "modificarCliente";
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@nombre", SqlDbType.NVarChar).Value = txtNombre.Text;
                cmd.Parameters.Add("@Tipo", SqlDbType.Int).Value = Int32.Parse(ddlTipo.SelectedItem.Value);
                cmd.Parameters.Add("@FechaModi", SqlDbType.Date).Value = modificar;
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            Response.Redirect("listCliente.aspx");
        }
    }
}