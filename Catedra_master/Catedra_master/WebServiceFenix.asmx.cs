﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace Catedra_master
{
    /// <summary>
    /// Descripción breve de WebServiceFenix
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceFenix : System.Web.Services.WebService
    {
        ClaseLinqDataContext clase = new ClaseLinqDataContext();

        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        [WebMethod]

        public Boolean ingresarFactura(int IDCliente,int IDComprador, int IDProveedor, string TMovimiento)
        {
            // Crear un nuevo objeto Actividad              
            Factura act = new Factura
            {
                IDCliente = IDCliente,
                IDComprador = IDComprador,
                IDProveedor = IDProveedor,
                TMovimiento = TMovimiento

            };
            // Agregar el nuevo objeto a collecion de consulta              
            clase.Factura.InsertOnSubmit(act);
            // Verifica si hay cambios en la base de datos              
            try
            {
                clase.SubmitChanges(); return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e); return false;
            }
        }
    }
}
