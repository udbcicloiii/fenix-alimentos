﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="listCliente.aspx.cs" Inherits="Catedra_master.listCliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
 <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li runat="server" id="nuevoCliente"><a href="nuevoCliente.aspx" class="button
special">Nuevo Cliente</a></li>
     <li runat="server" id="listClienteBaja"><a href="listClienteBaja.aspx" class="button
special">Clientes dados de Baja</a></li>
     <li runat="server" id="SolicitudCliente"><a href="SolicitudCliente.aspx" class="button
special">Solicitud de Cliente</a></li>
      </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
    <header class="major">
 <h2>Lista de clientes</h2>
 <p>FÉNIX Alimentos</p>
 </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row uniform 50%">
     <div class="4u 12u$(xsmall)">
 Tipo
<div class="select-wrapper">
 <asp:DropDownList
 ID="ddlTipo" runat="server">
     <asp:ListItem Selected="True" Value="1">Persona jurídica</asp:ListItem>
     <asp:ListItem Value="2">Persona natural</asp:ListItem>
 </asp:DropDownList>
 </div>
 </div>
<div class="2u 12u$(xsmall)">
 <br />
 <asp:Button ID="btnBuscar" runat="server"
 Text="Buscar" Width="100%" CssClass="special"
OnClick="btnBuscar_Click" />
 </div>
         <div class="2u 12u$(xsmall)">
             <br />
             <asp:Button ID="BtnLimpiar" runat="server" Text="Limpiar" Width="100%" CssClass="special" OnClick="BtnLimpiar_Click"/>
             </div>
<asp:GridView ID="GridView1" runat="server" 
    DataKeyNames="ID"
    OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
    OnRowDeleting="GridView1_RowDeleting">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/edit_flat_icon.png" HeaderText="Editar" ShowSelectButton="True" SelectImageUrl="~/images/edit_flat_icon.png">
            <controlstyle width="50px" />
        </asp:CommandField>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/eliminar_icon_flat.png" HeaderText="Dar de baja" ShowDeleteButton="True" DeleteImageUrl="~/images/eliminar_icon_flat.png" SelectImageUrl="~/images/eliminar_icon_flat.png">
            <controlstyle width="40px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>
 </div>
    <br />
    <div runat="server" id="titulosolicitud">
          <h2 style="text-align:center">Lista de solicitudes de clientes</h2>
    </div>
    <br />
    <div class="row uniform 50%">
     <div id="combo" runat="server" class="4u 12u$(xsmall)">
 Tipo
<div class="select-wrapper">
 <asp:DropDownList
 ID="ddlTipo2" runat="server">
     <asp:ListItem Selected="True" Value="1">Persona jurídica</asp:ListItem>
     <asp:ListItem Value="2">Persona natural</asp:ListItem>
 </asp:DropDownList>
 </div>
 </div>
<div class="2u 12u$(xsmall)">
 <br />
 <asp:Button ID="Button1" runat="server"
 Text="Buscar" Width="100%" CssClass="special"
OnClick="btnBuscar_Click2" />
 </div>
         <div class="2u 12u$(xsmall)">
             <br />
             <asp:Button ID="Button2" runat="server" Text="Limpiar" Width="100%" CssClass="special" OnClick="BtnLimpiar_Click2"/>
             </div>
<asp:GridView ID="GridView2" runat="server" 
    DataKeyNames="ID"
    OnSelectedIndexChanging="GridView2_SelectedIndexChanging"
    OnRowDeleting="GridView2_RowDeleting">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/check.png" HeaderText="Aprobar" ShowSelectButton="True" SelectImageUrl="~/images/check.png">
            <controlstyle width="50px" />
        </asp:CommandField>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/eliminar_icon_flat.png" HeaderText="Dar de baja" ShowDeleteButton="True" DeleteImageUrl="~/images/eliminar_icon_flat.png" SelectImageUrl="~/images/eliminar_icon_flat.png">
            <controlstyle width="40px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>
 </div>
</asp:Content>
