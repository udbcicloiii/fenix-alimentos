﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Catedra_master.Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="row uniform 50%">
                        <asp:ValidationSummary 
            ID="ValidationSummary1" 
            runat="server" 
            HeaderText="Verifique los siguientes errores....." 
            ShowMessageBox="false" 
            DisplayMode="BulletList" 
            ShowSummary="true"
            Width="650"
            Font-Size="X-Large"
            Font-Italic="true"
            />
                    </div>
                    <div class="row uniform 50%">
                        <div class="6u 12u$(xsmall)">
                            <asp:TextBox ID="txtEmail1" runat="server"
                                placeholder="Usuario"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic"
                                ID="RequiredFieldValidator3" runat="server" ErrorMessage="Username es Requerido"
                                ControlToValidate="txtEmail1" ></asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator  runat="server"  ControlToValidate="txtEmail1"
            ErrorMessage="Formato de entrada incorrecto" ValidationExpression="^[a-zA-Z]{1,20}$" ForeColor="#ff490d"></asp:RegularExpressionValidator>
                        </div>
                        <div class="6u 12u$(xsmall)">
                            <asp:TextBox ID="txtPass" runat="server"
                                placeholder="Contraseña" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ErrorMessage="Contraseña es Requerido"
                                ControlToValidate="txtPass"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator  runat="server"  ControlToValidate="txtPass"
            ErrorMessage="Formato de entrada incorrecto" ValidationExpression="^[A-Za-z0-9]{1,20}$" ForeColor="#ff490d"></asp:RegularExpressionValidator>
                        </div>
                        <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
                        <div class="12u$">
                            <ul class="actions">
                                <li>
                                    <asp:Button ID="btnEnviar" runat="server"
                                        Text="Iniciar Sesión" CssClass="special" OnClick="btnEnviar_Click" />
                                </li>
                            </ul>
                        </div>
                    </div>
</asp:Content>
