﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class listProducto : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            try
            {
                //Validando que se haya iniciado sesión
                //Escondiendo todas las opciones
                nuevoProductoSolicitud.Visible = false;
                nuevoProducto.Visible = false;
                listProductoBaja.Visible = false;
                //Obteniendo las variables de sesión
                string idRol = Session["TipoUsuario"].ToString();
                //Mostrando las opciones acorde al rol
                if (idRol == "1")//Administradores
                {
                  
                    nuevoProducto.Visible = true;
                    listProductoBaja.Visible = true;

                    if (IsPostBack == false)
                    {
                        //obtiene los productos dados de alta en el gridview
                        using (conn)
                        {

                            //Combobox de busqueda de productos
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getProveedor";
                            cmd.Connection = conn;
                            conn.Open();
                            ddlProveedor.DataSource = cmd.ExecuteReader();
                            ddlProveedor.DataTextField = "Nombre";
                            ddlProveedor.DataValueField = "id";
                            ddlProveedor.DataBind();
                            conn.Close();

                            //Combobox de busqueda de solicitudes

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getProveedor";
                            cmd.Connection = conn;
                            conn.Open();
                            ddlProveedor2.DataSource = cmd.ExecuteReader();
                            ddlProveedor2.DataTextField = "Nombre";
                            ddlProveedor2.DataValueField = "id";
                            ddlProveedor2.DataBind();
                            conn.Close();

                            //obtiene los usuario dados de alta en el gridview
                            cmd.CommandText = "getProductosAlta";
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();

                            //obtiene los usuario dados de alta en el gridview
                            cmd.CommandText = "getProductosAltaSolicitud";
                            conn.Open();
                            GridView2.DataSource = cmd.ExecuteReader();
                            GridView2.DataBind();
                            conn.Close();
                        }

                    }

                }
                else if (idRol == "2")//Jefes de Inventario
                {
                   
                    nuevoProducto.Visible = true;
                    listProductoBaja.Visible = true;

                    if (IsPostBack == false)
                    {
                        //obtiene los productos dados de alta en el gridview
                        using (conn)
                        {

                            //Combobox de busqueda de productos
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getProveedor";
                            cmd.Connection = conn;
                            conn.Open();
                            ddlProveedor.DataSource = cmd.ExecuteReader();
                            ddlProveedor.DataTextField = "Nombre";
                            ddlProveedor.DataValueField = "id";
                            ddlProveedor.DataBind();
                            conn.Close();

                            //Combobox de busqueda de solicitudes

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getProveedor";
                            cmd.Connection = conn;
                            conn.Open();
                            ddlProveedor2.DataSource = cmd.ExecuteReader();
                            ddlProveedor2.DataTextField = "Nombre";
                            ddlProveedor2.DataValueField = "id";
                            ddlProveedor2.DataBind();
                            conn.Close();

                            //obtiene los usuario dados de alta en el gridview
                            cmd.CommandText = "getProductosAlta";
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();

                            //obtiene los usuario dados de alta en el gridview
                            cmd.CommandText = "getProductosAltaSolicitud";
                            conn.Open();
                            GridView2.DataSource = cmd.ExecuteReader();
                            GridView2.DataBind();
                            conn.Close();
                        }

                    }
                }
                else if (idRol == "6")//Usuarios de Inventario
                {
                    nuevoProductoSolicitud.Visible = true;
                    ddlProveedor2.Visible = false;
                    Button1.Visible = false;
                    Button2.Visible = false;
                    tituloproveedor.Visible = false;
                    GridView1.Columns[1].Visible = false;
                    titulosolicitud.Visible = false;
                    if (IsPostBack == false)
                    {
                        //obtiene los productos dados de alta en el gridview
                        using (conn)
                        {

                            //Combobox de busqueda de productos
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getProveedor";
                            cmd.Connection = conn;
                            conn.Open();
                            ddlProveedor.DataSource = cmd.ExecuteReader();
                            ddlProveedor.DataTextField = "Nombre";
                            ddlProveedor.DataValueField = "id";
                            ddlProveedor.DataBind();
                            conn.Close();

                            //obtiene los usuario dados de alta en el gridview
                            cmd.CommandText = "getProductosAlta";
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();
                        }

                    }
                }
            }
            catch (Exception error)
            {
                Response.Redirect("menu.aspx");
            }
           
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Busca productos por medio del proveedor
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "BusquedaProducto";
                //obtiene los usuario dados de alta en el gridview
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = Int32.Parse(ddlProveedor.SelectedItem.Value);
                cmd.Connection = conn;
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
        }
        protected void btnBuscar_Click2(object sender, EventArgs e)
        {
            //Busca productos por medio del proveedor
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "BusquedaProductoSolicitud";
                //obtiene los usuario dados de alta en el gridview
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = Int32.Parse(ddlProveedor2.SelectedItem.Value);
                cmd.Connection = conn;
                conn.Open();
                GridView2.DataSource = cmd.ExecuteReader();
                GridView2.DataBind();
                conn.Close();
            }
        }
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Da de baja a productos mediante su campo estado designado como '2'
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");//obtiene fecha actual
            
            try
            {
                //Da de baja al producto y actualiza el GridView
                id = int.Parse(GridView1.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarProductoEstado";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 2;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                Response.Redirect("listProducto.aspx");
            }
            catch (Exception ex)
            {

            }
        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de productos obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("editarProducto.aspx?id={0}", nIdEmpleado));
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Da de baja a productos mediante su campo estado designado como '2'
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");//obtiene fecha actual

            try
            {
                //Da de baja al producto y actualiza el GridView
                id = int.Parse(GridView2.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarProductoEstado";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 2;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                Response.Redirect("listProducto.aspx");
            }
            catch (Exception ex)
            {

            }
        }

        protected void GridView2_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de productos obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView2.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("AprobarSolicitudProducto.aspx?id={0}", nIdEmpleado));
        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            //Resetea los datos buscados
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "getProveedor";
                cmd.Connection = conn;
                conn.Open();
                ddlProveedor.DataSource = cmd.ExecuteReader();
                ddlProveedor.DataTextField = "Nombre";
                ddlProveedor.DataValueField = "id";
                ddlProveedor.DataBind();
                conn.Close();

                //obtiene los usuario dados de alta en el gridview
                cmd.CommandText = "getProductosAlta";
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
        }
        protected void BtnLimpiar_Click2(object sender, EventArgs e)
        {
            //Resetea los datos buscados
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "getProveedor";
                cmd.Connection = conn;
                conn.Open();
                ddlProveedor2.DataSource = cmd.ExecuteReader();
                ddlProveedor2.DataTextField = "Nombre";
                ddlProveedor2.DataValueField = "id";
                ddlProveedor2.DataBind();
                conn.Close();

                //obtiene los usuario dados de alta en el gridview
                cmd.CommandText = "getProductosAltaSolicitud";
                conn.Open();
                GridView2.DataSource = cmd.ExecuteReader();
                GridView2.DataBind();
                conn.Close();
            }
        }
    }
}