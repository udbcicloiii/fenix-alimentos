﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Catedra_master
{
    public partial class nuevoUsuario : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            if (IsPostBack == false)
            {
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getTipoUsuario";
                    cmd.Connection = conn;
                    conn.Open();
                    ddlTipoUsuario.DataSource = cmd.ExecuteReader();
                    ddlTipoUsuario.DataTextField = "Cargo";
                    ddlTipoUsuario.DataValueField = "ID";
                    ddlTipoUsuario.DataBind();
                    conn.Close();
                }

            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Obtiene la fecha actual e inserta datos de usuario
            String actual = DateTime.Now.ToString("yyyy-MM-dd");
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "insertarUsuario";
                cmd.Parameters.Add("@Nombre", SqlDbType.NVarChar).Value = txtUsuario.Text;
                cmd.Parameters.Add("@Contraseña", SqlDbType.NVarChar).Value = txtContraseña.Text;
                cmd.Parameters.Add("@IDTipoUsuario", SqlDbType.Int).Value = Int32.Parse(ddlTipoUsuario.SelectedItem.Value);
                cmd.Parameters.Add("@FechaCrea", SqlDbType.Date).Value = actual; 
                cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 1;
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
                Response.Redirect("listUsuario.aspx");
            }

    }
}