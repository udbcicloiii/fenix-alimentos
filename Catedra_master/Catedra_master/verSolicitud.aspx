﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="verSolicitud.aspx.cs" Inherits="Catedra_master.verSolicitud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
    <style>
        .titulos{
            font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
            font-size: 1em;
            margin-left: 41%;
        }

        .lblCompras{
            font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
            font-size: 1.7em;
            margin-left: 38%;
        }
    </style>
 <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <header class="major">
 <h2>Solicitud</h2>
 <p>FÉNIX Alimentos</p>
 </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <asp:Label ID="lblCompras" CssClass="lblCompras" runat="server" Text="Solicitudes Compras"></asp:Label>
    <asp:Label ID="lbl1" CssClass="titulos" runat="server" Text="Solicitudes por Aprobadas"></asp:Label>
            <br />

    <div class="row uniform 50%">
          <br />
<asp:GridView ID="GridView1" runat="server" 
    DataKeyNames="id,IDComprador,IDProveedor"
    OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
    OnRowDeleting="GridView1_RowDeleting">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/check_flat_icon.png" HeaderText="Aprobar" ShowSelectButton="True" SelectImageUrl="~/images/check_flat_icon.png" EditText="Ver">
            <controlstyle width="50px" />
        </asp:CommandField>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/eliminar_icon_flat.png" HeaderText="Eliminar" ShowDeleteButton="True" DeleteImageUrl="~/images/eliminar_icon_flat.png" SelectImageUrl="~/images/eliminar_icon_flat.png">
            <controlstyle width="40px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>
        
        </div>
     
<asp:Label ID="lbl2" CssClass="titulos" runat="server" Text="Solicitudes Desaprobadas"></asp:Label>
  
        <div class="row uniform 50%">
             <br />
<asp:GridView ID="GridView2" runat="server" 
    DataKeyNames="Id">
     </asp:GridView>
            <linea></linea>
           
 </div>
     <br />
     <br />
  <asp:Label ID="Label1" CssClass="lblCompras" runat="server" Text="Solicitudes Ventas"></asp:Label>
    <asp:Label ID="Label3" CssClass="titulos" runat="server" Text="Solicitudes por Aprobadas"></asp:Label>
    <div class="row uniform 50%">
          <br />
        
<asp:GridView ID="GridView3" runat="server" 
    DataKeyNames="id,IDCliente,IDProveedor"
    OnSelectedIndexChanging="GridView3_SelectedIndexChanging"
    OnRowDeleting="GridView3_RowDeleting">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/check_flat_icon.png" HeaderText="Aprobar" ShowSelectButton="True" SelectImageUrl="~/images/check_flat_icon.png" EditText="Ver">
            <controlstyle width="50px" />
        </asp:CommandField>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/eliminar_icon_flat.png" HeaderText="Eliminar" ShowDeleteButton="True" DeleteImageUrl="~/images/eliminar_icon_flat.png" SelectImageUrl="~/images/eliminar_icon_flat.png">
            <controlstyle width="40px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>
        
        </div>
    <asp:Label ID="Label2" CssClass="titulos" runat="server" Text="Solicitudes Desaprobadas"></asp:Label>
    <div class="row uniform 50%">
             <br />
<asp:GridView ID="GridView4" runat="server" 
    DataKeyNames="Id">
     </asp:GridView>
           
 </div>
</asp:Content>
