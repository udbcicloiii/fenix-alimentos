﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class FacturasTodas : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
           
                    //Cargando los datos de empleados al gridview
                    if (IsPostBack == false)
                    {
                        using (conn)
                        {
                    //obtiene los usuario dados de alta en el gridview
                             cmd.CommandText = "getTotalFacturas";
                                     cmd.Connection = conn;
                                     conn.Open();
                                     GridView1.DataSource = cmd.ExecuteReader();
                                     GridView1.DataBind();
                                     conn.Close();
                                 }

                    }
               
            
            


        }


        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

     
    }
}