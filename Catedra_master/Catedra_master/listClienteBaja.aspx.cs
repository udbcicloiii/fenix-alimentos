﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Catedra_master
{
    public partial class listClienteBaja : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            //Cargando los datos de empleados al gridview
            if (IsPostBack == false)
            {
                //Obtiene los clientes los cuales estan dados de baja
                using (conn)
                {
                    //obtiene los usuario dados de baja en el gridview
                    cmd.CommandText = "getClientes2";
                    cmd.Connection = conn;
                    conn.Open();
                    GridView1.DataSource = cmd.ExecuteReader();
                    GridView1.DataBind();
                    conn.Close();

                    //obtiene los usuario dados de baja en el gridview
                    cmd.CommandText = "getClientes2Solicitud";
                    cmd.Connection = conn;
                    conn.Open();
                    GridView2.DataSource = cmd.ExecuteReader();
                    GridView2.DataBind();
                    conn.Close();
                }
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Obtiene fecha actual
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");
            try
            {
                //Dando de alta a clientes y actualizando el GridView
                //se le asina el estado '1' que significa dado de alta
                id = int.Parse(GridView1.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarClienteEstado";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 1;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    Response.Redirect("listClienteBaja.aspx");
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de clientes obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("editarCliente.aspx?id={0}", nIdEmpleado));
        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Obtiene fecha actual
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");
            try
            {
                //Dando de alta a clientes y actualizando el GridView
                //se le asina el estado '1' que significa dado de alta
                id = int.Parse(GridView2.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarClienteEstado";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    Response.Redirect("listClienteBaja.aspx");
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void GridView2_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de clientes obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView2.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("editarCliente.aspx?id={0}", nIdEmpleado));
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Busca clientes por el tipo de persona y que sean dados de baja
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "BusquedaCliente2";
                //obtiene los usuario dados de baja en el gridview
                cmd.Parameters.Add("@Tipo", SqlDbType.Int).Value = Int32.Parse(ddlTipo.SelectedItem.Value);
                cmd.Connection = conn;
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
        }

        protected void btnBuscar_Click2(object sender, EventArgs e)
        {
            //Busca clientes por el tipo de persona y que sean dados de baja
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "BusquedaCliente2Solicitud";
                //obtiene los usuario dados de baja en el gridview
                cmd.Parameters.Add("@Tipo", SqlDbType.Int).Value = Int32.Parse(ddlTipo2.SelectedItem.Value);
                cmd.Connection = conn;
                conn.Open();
                GridView2.DataSource = cmd.ExecuteReader();
                GridView2.DataBind();
                conn.Close();
            }
        }


        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            //limpia los datos buscados
            using (conn)
            {
                //obtiene los clientes dados de baja en el gridview
                cmd.CommandText = "getClientes2";
                cmd.Connection = conn;
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
        }

        protected void BtnLimpiar_Click2(object sender, EventArgs e)
        {
            //limpia los datos buscados
            using (conn)
            {
                //obtiene los clientes dados de baja en el gridview
                cmd.CommandText = "getClientes2Solicitud";
                cmd.Connection = conn;
                conn.Open();
                GridView2.DataSource = cmd.ExecuteReader();
                GridView2.DataBind();
                conn.Close();
            }
        }
    }
}