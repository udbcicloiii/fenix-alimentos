﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class facturacion : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmd2 = new SqlCommand();

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        int id = 0;
        int id2 = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }

            if (Request.QueryString["idcomprador"] != null)
            {
                id = int.Parse(Request.QueryString["idcomprador"]);//obtiene el id de la transacción
            }
           if(Request.QueryString["idcliente"] != null)
            {
                id2 = int.Parse(Request.QueryString["idcliente"]);//obtiene el id de la transacción
            }
           
            if (id2 != 0)
            {
                if (!Page.IsPostBack)
                {
                    //Rellena datos en el dropdownlist con la lista completa de proveedores
                    using (conn2)
                    {
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.CommandText = "DetalleFacturaVenta";
                        //obtiene los usuario dados de alta en el gridview
                        cmd2.Parameters.Add("@id", SqlDbType.Int).Value = id2;
                        cmd2.Connection = conn2;
                        conn2.Open();
                        GridView1.DataSource = cmd2.ExecuteReader();
                        GridView1.DataBind();
                        conn2.Close();
                    }
                }
            }
            if (id != 0)
            {
                if (!Page.IsPostBack)
                {
                    //Rellena datos en el dropdownlist con la lista completa de proveedores
                    using (conn)
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "DetalleFacturaCompra";
                        //obtiene los usuario dados de alta en el gridview
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        cmd.Connection = conn;
                        conn.Open();
                        GridView1.DataSource = cmd.ExecuteReader();
                        GridView1.DataBind();
                        conn.Close();
                    }
                }
            }
           
        }
        protected void btnAprobar_Click(object sender, EventArgs e)
        {
            Response.Redirect("verSolicitud.aspx");
        }
    }
}