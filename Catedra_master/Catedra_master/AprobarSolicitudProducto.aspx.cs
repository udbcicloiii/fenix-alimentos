﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class AprobarSolicitudProducto : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmd2 = new SqlCommand();
        SqlCommand cmd3 = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        int id = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            ddlProveedor.Enabled = false;//Se desabilita en dropdownlist
            id = int.Parse(Request.QueryString["id"]);//obtiene el id del producto a editar
            int prov;
            if (!Page.IsPostBack)
            {
                //Rellena datos en el dropdownlist con la lista completa de proveedores
                using (conn)
                {
                    //Obtiene el proveedor actual del producto
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getProductosID";
                    cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dr.Read();
                    txtNombre.Text = Convert.ToString(dr["Nombre"]).TrimEnd();
                    txtPrecioCompra.Text = Convert.ToString(dr["Precio de compra"]);
                    txtPrecioVenta.Text = Convert.ToString(dr["Precio de venta"]);
                    txtCant.Text = Convert.ToString(dr["Cantidad"]);
                    prov = Convert.ToInt32(dr["Proveedor"]);
                    conn.Close();

                    //Obtener proveedor actual
                    cmd3.CommandType = CommandType.StoredProcedure;
                    cmd3.CommandText = "getNombreProveedor";
                    cmd3.Parameters.Add("@id", SqlDbType.Int).Value = prov;
                    cmd3.Connection = conn;
                    conn.Open();
                    ddlProveedor.DataSource = cmd3.ExecuteReader();
                    ddlProveedor.DataTextField = "Nombre";
                    ddlProveedor.DataBind();
                    conn.Close();

                    //Mostrar todos los proveedores
                    cmd2.CommandText = "getProveedor";
                    cmd2.Connection = conn;
                    conn.Open();
                    DropDownList1.DataSource = cmd2.ExecuteReader();
                    DropDownList1.DataTextField = "Nombre";
                    DropDownList1.DataValueField = "ID";
                    DropDownList1.DataBind();
                    conn.Close();
                }
            }
        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            // Validar precio de venta
            if (float.Parse(txtPrecioVenta.Text) > float.Parse(txtPrecioCompra.Text))
            {
                //Obtiene la fecha actual y modifica el producto
                String modificar = DateTime.Now.ToString("yyyy-MM-dd");
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SolicitudAprobadaProducto";
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@Nombre", SqlDbType.NVarChar).Value = txtNombre.Text;
                    cmd.Parameters.Add("@IDProveedor", SqlDbType.Int).Value = Int32.Parse(DropDownList1.SelectedItem.Value);
                    cmd.Parameters.Add("@PrecioCompra", SqlDbType.Decimal).Value = float.Parse(txtPrecioCompra.Text);
                    cmd.Parameters.Add("@PrecioVenta", SqlDbType.Decimal).Value = float.Parse(txtPrecioVenta.Text);
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Int).Value = Int32.Parse(txtCant.Text);
                    cmd.Parameters.Add("@FechaModi", SqlDbType.Date).Value = modificar;

                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                Response.Redirect("listProducto.aspx");
            }
            else
            {
                lblerror.Text = "El precio de venta a ingresar debe ser mayor al precio de compra para obtener ganancias.";
            }
        }
    }
}