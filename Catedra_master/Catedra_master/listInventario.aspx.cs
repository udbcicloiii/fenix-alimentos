﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class listInventario : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            try
            {
                //Validando que se haya iniciado sesión
                //Escondiendo todas las opciones
                nuevoMovimiento.Visible = false;
                nuevaBodega.Visible = false;
                //Obteniendo las variables de sesión
                string idRol = Session["TipoUsuario"].ToString();
                int usuario = (int)Session["id"];
                //Mostrando las opciones acorde al rol
                if (idRol == "1")//Administradores
                {
             
                    nuevaBodega.Visible = true;
                    //Cargando los datos de empleados al gridview
                    if (IsPostBack == false)
                    {
                        using (conn)
                        {
                            //Carga los datos de bodegas al dropdownlist
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getBodega";
                            cmd.Connection = conn;
                            conn.Open();
                            ddlBodega.DataSource = cmd.ExecuteReader();
                            ddlBodega.DataTextField = "Nombre";
                            ddlBodega.DataValueField = "id";
                            ddlBodega.DataBind();
                            conn.Close();

                            //Carga los datos de productos al dropdownlist
                            cmd.CommandText = "getProductos";
                            cmd.Connection = conn;
                            conn.Open();
                            ddlProducto.DataSource = cmd.ExecuteReader();
                            ddlProducto.DataTextField = "Nombre";
                            ddlProducto.DataValueField = "id";
                            ddlProducto.DataBind();
                            conn.Close();

                            //obtiene el inventario en el gridview
                            cmd.CommandText = "getInventario";
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();
                        }
                    }
                }
                else if (idRol == "2")//Jefes de Inventario
                {
               
                    nuevaBodega.Visible = true;
                    //Cargando los datos de empleados al gridview
                    if (IsPostBack == false)
                    {
                        using (conn)
                        {
                            //Carga los datos de bodegas al dropdownlist
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getBodega";
                            cmd.Connection = conn;
                            conn.Open();
                            ddlBodega.DataSource = cmd.ExecuteReader();
                            ddlBodega.DataTextField = "Nombre";
                            ddlBodega.DataValueField = "id";
                            ddlBodega.DataBind();
                            conn.Close();

                            //Carga los datos de productos al dropdownlist
                            cmd.CommandText = "getProductos";
                            cmd.Connection = conn;
                            conn.Open();
                            ddlProducto.DataSource = cmd.ExecuteReader();
                            ddlProducto.DataTextField = "Nombre";
                            ddlProducto.DataValueField = "id";
                            ddlProducto.DataBind();
                            conn.Close();

                            //obtiene el inventario en el gridview
                            cmd.CommandText = "getInventario";
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();
                        }
                    }
                }
                else if (idRol == "6")//Usuarios de Inventario
                {
                    nuevoMovimiento.Visible = true;
                  
                    //Cargando los datos de empleados al gridview
                    if (IsPostBack == false)
                    {
                        using (conn)
                        {
                            //Carga los datos de bodegas al dropdownlist
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getBodega";
                            cmd.Connection = conn;
                            conn.Open();
                            ddlBodega.DataSource = cmd.ExecuteReader();
                            ddlBodega.DataTextField = "Nombre";
                            ddlBodega.DataValueField = "id";
                            ddlBodega.DataBind();
                            conn.Close();

                            //Carga los datos de productos al dropdownlist
                            cmd.CommandText = "getProductos";
                            cmd.Connection = conn;
                            conn.Open();
                            ddlProducto.DataSource = cmd.ExecuteReader();
                            ddlProducto.DataTextField = "Nombre";
                            ddlProducto.DataValueField = "id";
                            ddlProducto.DataBind();
                            conn.Close();

                            //obtiene el inventario en el gridview
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "BusquedaInventarioPrivilegio6";
                            //obtiene los usuario dados de alta en el gridview
                            cmd.Parameters.Add("@usuario", SqlDbType.NVarChar).Value = usuario;
                            cmd.Connection = conn;
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();
                        }
                    }
                }
            }
            catch (Exception error)
            {
                Response.Redirect("menu.aspx");
            }

            
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            String Tmovimiento = (ddlTipoMovimiento.SelectedItem.Value);
            int Producto = Int32.Parse(ddlProducto.SelectedItem.Value);
            int Bodega = Int32.Parse(ddlBodega.SelectedItem.Value);
            //Busca por medio del movimiento,producto y bodega
            string idRol = Session["TipoUsuario"].ToString();
            int usuario = (int)Session["id"];
            //Mostrando las opciones acorde al rol
            if (idRol == "1")//Administradores
            {
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "BusquedaInventario";
                    //obtiene los usuario dados de alta en el gridview
                    cmd.Parameters.Add("@TMovimiento", SqlDbType.NVarChar).Value = Tmovimiento;
                    cmd.Parameters.Add("@IDProducto", SqlDbType.Int).Value = Producto;
                    cmd.Parameters.Add("@IDBodega", SqlDbType.Int).Value = Bodega;
                    cmd.Connection = conn;
                    conn.Open();
                    GridView1.DataSource = cmd.ExecuteReader();
                    GridView1.DataBind();
                    conn.Close();
                }
            }
            else if (idRol == "2")//Jefes de Inventario
            {
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "BusquedaInventario";
                    //obtiene los usuario dados de alta en el gridview
                    cmd.Parameters.Add("@TMovimiento", SqlDbType.NVarChar).Value = Tmovimiento;
                    cmd.Parameters.Add("@IDProducto", SqlDbType.Int).Value = Producto;
                    cmd.Parameters.Add("@IDBodega", SqlDbType.Int).Value = Bodega;
                    cmd.Connection = conn;
                    conn.Open();
                    GridView1.DataSource = cmd.ExecuteReader();
                    GridView1.DataBind();
                    conn.Close();
                }
            }
            else if (idRol == "6")//Usuarios de Inventario
            {
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "BusquedaInventarioPrivilegio62";
                    //obtiene los usuario dados de alta en el gridview
                    cmd.Parameters.Add("@TMovimiento", SqlDbType.NVarChar).Value = Tmovimiento;
                    cmd.Parameters.Add("@IDProducto", SqlDbType.Int).Value = Producto;
                    cmd.Parameters.Add("@IDBodega", SqlDbType.Int).Value = Bodega;
                    cmd.Parameters.Add("@usuario", SqlDbType.NVarChar).Value = usuario;
                    cmd.Connection = conn;
                    conn.Open();
                    GridView1.DataSource = cmd.ExecuteReader();
                    GridView1.DataBind();
                    conn.Close();
                }
            }

            }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            //Limpia los datos buscados
            using (conn)
            {
                //Carga los datos de bodegas al dropdownlist
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "getBodega";
                cmd.Connection = conn;
                conn.Open();
                ddlBodega.DataSource = cmd.ExecuteReader();
                ddlBodega.DataTextField = "Nombre";
                ddlBodega.DataValueField = "id";
                ddlBodega.DataBind();
                conn.Close();

                //Carga los datos de productos al dropdownlist
                cmd.CommandText = "getProductos";
                cmd.Connection = conn;
                conn.Open();
                ddlProducto.DataSource = cmd.ExecuteReader();
                ddlProducto.DataTextField = "Nombre";
                ddlProducto.DataValueField = "id";
                ddlProducto.DataBind();
                conn.Close();

                string idRol = Session["TipoUsuario"].ToString();
                int usuario = (int)Session["id"];
                if (idRol == "6")
                {
                    //obtiene el inventario en el gridview
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "BusquedaInventarioPrivilegio6";
                    //obtiene los usuario dados de alta en el gridview
                    cmd.Parameters.Add("@usuario", SqlDbType.NVarChar).Value = usuario;
                    cmd.Connection = conn;
                    conn.Open();
                    GridView1.DataSource = cmd.ExecuteReader();
                    GridView1.DataBind();
                    conn.Close();
                }
                else
                {
                //obtiene el inventario en el gridview
                cmd.CommandText = "getInventario";
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
            }
        }

    }
}