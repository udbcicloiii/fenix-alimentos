﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="FacturasTodas.aspx.cs" Inherits="Catedra_master.FacturasTodas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
 <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>

 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <header class="major">
 <h2>Lista de Inventario</h2>
 <p>FÉNIX Alimentos</p>
 </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row uniform 50%">
        
<asp:GridView ID="GridView1" runat="server" 
  DataKeyNames="ID"
    OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
    OnRowDeleting="GridView1_RowDeleting" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
     </asp:GridView>
 </div>
</asp:Content>