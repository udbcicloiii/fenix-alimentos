﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Catedra_master
{
    public partial class listProveedorBaja : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            //Cargando los datos de proveedor dados de baja al gridview
            if (IsPostBack == false)
            {
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getProveedor2";
                    cmd.Connection = conn;
                    conn.Open();
                    GridView1.DataSource = cmd.ExecuteReader();
                    GridView1.DataBind();
                    conn.Close();

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getProveedor2Solicitud";
                    cmd.Connection = conn;
                    conn.Open();
                    GridView2.DataSource = cmd.ExecuteReader();
                    GridView2.DataBind();
                    conn.Close();
                }
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");//Obtiene la fecha actual
            try
            {
                //Dando de alta al proveedor seleccionado y actualizando el GridView
                id = int.Parse(GridView1.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarProveedor";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 1;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                
            }
            catch (Exception ex)
            {

            }
            Response.Redirect("listProveedorBaja.aspx");
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de proveedores, obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("EditarProveedor.aspx?id={0}", nIdEmpleado));
        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");//Obtiene la fecha actual
            try
            {
                //Dando de alta al proveedor seleccionado y actualizando el GridView
                id = int.Parse(GridView2.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarProveedor";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

            }
            catch (Exception ex)
            {

            }
            Response.Redirect("listProveedorBaja.aspx");
        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView2_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de proveedores, obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView2.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("EditarProveedor.aspx?id={0}", nIdEmpleado));
        }
    }
}