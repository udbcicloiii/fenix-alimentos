﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="OrdenarCompra.aspx.cs" Inherits="Catedra_master.OrdenarCompra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <!-- Header -->
    <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li><a href="Compra.aspx" class="button
special">Ver productos</a></li>
 </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <h3>Ordenar Productos</h3>
 <p>
 Rellene los siguientes campos
 </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row uniform 50%">
 <div class="6u 12u$(xsmall)">
 <label>Nombre</label>
 <asp:TextBox ID="txtNombre"
 runat="server"></asp:TextBox>
 </div>
        <div class="6u 12u$(xsmall)">
 <label>Precio</label>
 <asp:TextBox ID="txtPrecio"
 runat="server"></asp:TextBox>
            </div>

        </div>
    <div class="row uniform 50%">
        <div class="6u 12u$(xsmall)">
 <label>Cantidad a ordenar</label>
 <asp:TextBox ID="txtCant"
 runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator
 ID="RequiredFieldValidator4"
ControlToValidate="txtCant" ForeColor="Red"
 runat="server" ErrorMessage="Cantidad obligatoria"></asp:RequiredFieldValidator>
 <asp:RegularExpressionValidator Display="Dynamic"
 ID="RegularExpressionValidator3" runat="server" ErrorMessage="Ingrese una cantidad correcta"
 ControlToValidate="txtCant" ValidationExpression="^[0-9]+$" ForeColor="#ff490d"></asp:RegularExpressionValidator>
            </div>
        <div class="6u 12u$(xsmall)">
 <label>Bodega</label>
 <div class="select-wrapper">
 <asp:DropDownList
 ID="ddlBodega" runat="server">
 </asp:DropDownList>
 </div>
 </div>
        </div>
    <br />
    <div class="6u 12u$(xsmall)">
        <asp:Label  id="lblerror" runat="server" ForeColor="#ff490d"></asp:Label>
    </div>
    <br />
    <div class="6u 12u$(xsmall)">
 <asp:Button ID="btnBuscar" runat="server"
 Text="Ordenar" CssClass="special"
OnClick="btnBuscar_Click" />
 </div>
</asp:Content>
