﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class VerCarrito : System.Web.UI.Page
    {
        //Instacias con la bdd
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmd2 = new SqlCommand();
        SqlCommand cmd3 = new SqlCommand();
        SqlCommand cmd4 = new SqlCommand();
        SqlCommand cmd5 = new SqlCommand();

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn3 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn4 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        int id = 0;
        String total1 = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //Verifica si existe un usuario
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            //Muestra la informacion del carrito
            using (conn)
            {
                cmd.CommandText = "VerCarrito";
                cmd.Connection = conn;
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();
            }
            total();
            //Si existe productos muestra total
            if (total1 == "")
            {
                Label1.Text = "Por favor ingrese productos al Carrito";
            }
            else
            {
                Label1.Text = "Total: $ " + total1;
            }

        }
        public void total()
        {

            using (conn2)
            {
                //Obtiene el monto total de los productos
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.CommandText = "ObtenerTotal";
                cmd2.Connection = conn2;
                conn2.Open();
                SqlDataReader dr = cmd2.ExecuteReader();
                dr.Read();
                total1 = Convert.ToString(dr["Precio"]);
                conn2.Close();
            }

        }
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            //Eliminar un producto del Carrito
            try
            {
                int id2 = int.Parse(GridView1.DataKeys[e.RowIndex].Values["ID"].ToString());
                //System.Web.HttpContext.Current.Response.Write("<script>alert("+id+")</script>");
                using (conn3)
                {
                    cmd3.CommandType = CommandType.StoredProcedure;
                    cmd3.CommandText = "EliminarProductoCarrito";
                    cmd3.Parameters.Add("@id", SqlDbType.Int).Value = id2;
                    cmd3.Connection = conn3;
                    conn3.Open();
                    cmd3.ExecuteNonQuery();
                    conn3.Close();
                }
                Response.Redirect("VerCarrito.aspx");
            }
            catch (SqlException ex)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert(" + ex + ")</script>");
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Actualiza el estado de compra para que se diriga al apartado de solicitudes
            using (conn4)
            {
                cmd4.CommandType = CommandType.StoredProcedure;
                cmd4.CommandText = "ActualizarEstadoCompra";
                cmd4.Parameters.Add("@aprob", SqlDbType.Int).Value = 0;
                cmd4.Parameters.Add("@id", SqlDbType.Int).Value = Session["UltimoCompra"];
                cmd4.Parameters.Add("@Proveedor", SqlDbType.Int).Value = Session["proveedor"];
                cmd4.Connection = conn4;
                conn4.Open();
                cmd4.ExecuteNonQuery();
                conn4.Close();

                cmd5.CommandType = CommandType.StoredProcedure;
                cmd5.CommandText = "InsertarMontoTotal";
                cmd5.Parameters.Add("@monto", SqlDbType.Decimal).Value = total1;
                cmd5.Parameters.Add("@id", SqlDbType.Int).Value = Session["UltimoCompra"];
                cmd5.Connection = conn4;
                conn4.Open();
                cmd5.ExecuteNonQuery();
                conn4.Close();
            }
            Response.Redirect("menu.aspx");
        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Obtiene el id Seleccionado
            id = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Value);
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}