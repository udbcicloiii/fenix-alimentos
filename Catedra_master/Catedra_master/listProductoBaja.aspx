﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="listProductoBaja.aspx.cs" Inherits="Catedra_master.listProductoBaja" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
   <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li><a href="nuevoProducto.aspx" class="button
special">Nuevo Producto</a></li>
     <li><a href="listProducto.aspx" class="button
special">Productos dados de alta</a></li>
      </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="titulo" runat="server">
    <header class="major">
 <h2>Lista de productos</h2>
 <p>FÉNIX Alimentos</p>
 </header>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row uniform 50%">
     <div class="4u 12u$(xsmall)">
 Proveedor
<div class="select-wrapper">
 <asp:DropDownList
 ID="ddlProveedor" runat="server">
 </asp:DropDownList>
 </div>
 </div>
<div class="2u 12u$(xsmall)">
 <br />
 <asp:Button ID="btnBuscar" runat="server"
 Text="Buscar" Width="100%" CssClass="special"
OnClick="btnBuscar_Click" />
 </div>
        <div class="2u 12u$(xsmall)">
             <br />
             <asp:Button ID="BtnLimpiar" runat="server"
 Text="Limpiar" Width="100%" CssClass="special" OnClick="BtnLimpiar_Click"/>
             </div>
<asp:GridView ID="GridView1" runat="server" 
    DataKeyNames="ID"
    OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
    OnRowDeleting="GridView1_RowDeleting" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/edit_flat_icon.png" HeaderText="Editar" ShowSelectButton="True" SelectImageUrl="~/images/edit_flat_icon.png">
            <controlstyle width="50px" />
        </asp:CommandField>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/up-arrow.png" HeaderText="Dar de alta" ShowDeleteButton="True" DeleteImageUrl="~/images/up-arrow.png" SelectImageUrl="~/images/up-arrow.png">
            <controlstyle width="40px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>
 </div>
     <div class="row uniform 50%">
     <div class="4u 12u$(xsmall)">
 Proveedor
<div class="select-wrapper">
 <asp:DropDownList
 ID="ddlProveedor2" runat="server">
 </asp:DropDownList>
 </div>
 </div>
<div class="2u 12u$(xsmall)">
 <br />
 <asp:Button ID="Button1" runat="server"
 Text="Buscar" Width="100%" CssClass="special"
OnClick="btnBuscar_Click2" />
 </div>
        <div class="2u 12u$(xsmall)">
             <br />
             <asp:Button ID="Button2" runat="server"
 Text="Limpiar" Width="100%" CssClass="special" OnClick="BtnLimpiar_Click2"/>
             </div>
<asp:GridView ID="GridView2" runat="server" 
    DataKeyNames="ID"
    OnSelectedIndexChanging="GridView2_SelectedIndexChanging"
    OnRowDeleting="GridView2_RowDeleting" OnSelectedIndexChanged="GridView2_SelectedIndexChanged">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/edit_flat_icon.png" HeaderText="Editar" ShowSelectButton="True" SelectImageUrl="~/images/edit_flat_icon.png">
            <controlstyle width="50px" />
        </asp:CommandField>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/up-arrow.png" HeaderText="Dar de alta" ShowDeleteButton="True" DeleteImageUrl="~/images/up-arrow.png" SelectImageUrl="~/images/up-arrow.png">
            <controlstyle width="40px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>
 </div>
</asp:Content>
