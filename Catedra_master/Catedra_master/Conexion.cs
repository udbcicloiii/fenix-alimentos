﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Catedra_master
{
    public class Conexion
    {
        //Declarando variables a utilizar en toda la clase
        private String cadenaConexion;
        private SqlConnection conexionSQL;
        private String consulta;
        private SqlCommand comando;
        private SqlDataAdapter adaptador;
        //Declarando el constructor de la clase
        public Conexion()
        {
            cadenaConexion = ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString;
        }
        //Función que conecta la bdd
        public bool conectar()
        {
            try
            {
                conexionSQL = new SqlConnection(cadenaConexion);
                conexionSQL.Open();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        //Función que devuelve el estado de la conexión
        public bool estadoConexion()
        {
            switch (conexionSQL.State)
            {
                case System.Data.ConnectionState.Broken:
                    return true;
                case System.Data.ConnectionState.Open:
                    return true;
                default:
                    return false;
            }
        }
        //Función que desconecta la base de datos
        public void desconectar()
        {
            conexionSQL.Close();
        }



//---------------------------------------------LOGIN------------------------------------------------------//

        //Función para validar el inicio de sesión
        public bool logIn(string user, string pass)
        {
            DataTable result = new DataTable();


            consulta = "EXEC LoginUser @user, @pass";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@user", user);
            comando.Parameters.AddWithValue("@pass", pass);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            if (result.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool logInCliente(string user, string pass)
        {
            DataTable result = new DataTable();


            consulta = "EXEC LoginUserCliente @user, @pass";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@user", user);
            comando.Parameters.AddWithValue("@pass", pass);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            if (result.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Funcion para obtener el ID con la persona logueada
        public DataTable getUsuario(String usuario)
        {
            DataTable result = new DataTable();
            consulta = "exec getUsuario @User";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@User", usuario);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            return result;
        }

        public DataTable getUsuarioCliente(String usuario)
        {
            DataTable result = new DataTable();
            consulta = "exec getUsuarioCliente @User";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@User", usuario);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            return result;
        }

        //---------------------------------------------PROVEEDOR------------------------------------------------------//

        //Insertar nuevo proveedor
        public int insertarProveedor(String Nombre, String Estado, String FechaCreacion,int aprob)
        {
            try
            {

                consulta = "exec insertarProveedor @Nombre,@Estado,@FechaCreacion,@Aprobacion";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@Nombre", Nombre);
                comando.Parameters.AddWithValue("@Estado", Estado);
                comando.Parameters.AddWithValue("@Aprobacion", aprob);
                comando.Parameters.AddWithValue("@FechaCreacion", FechaCreacion);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        //Obtener todos los datos de Proveedor para editar
        public DataTable getProveedorEditar(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec getProveedorEditar @ID";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@ID", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Estado";
            result.Columns[3].ColumnName = "Fecha Creación";
            result.Columns[4].ColumnName = "Fecha Modificación";

            return result;
        }

        //Busqueda de Proveedor
        public DataTable BusquedaProveedor(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec BusquedaProveedor @ID";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@ID", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "ID";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Estado";
            result.Columns[3].ColumnName = "FechaCrea";
            result.Columns[4].ColumnName = "FechaModi";

            return result;
        }


        //Obtener todos los datos de Proveedor por alta
        public DataTable getProveedor()
        {
            DataTable result = new DataTable();
            consulta = "exec getProveedor";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Estado";
            result.Columns[3].ColumnName = "Fecha Creación";
            result.Columns[4].ColumnName = "Fecha Modificación";

            return result;
        }

        //Obtener todos los datos de Proveedor por baja
        public DataTable getProveedor2()
        {
            DataTable result = new DataTable();
            consulta = "exec getProveedor2";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Estado";
            result.Columns[3].ColumnName = "Fecha Creación";
            result.Columns[4].ColumnName = "Fecha Modificación";

            return result;
        }


        //Obtiene el id del proveedor de la tabla productos
        public DataTable getProveedor(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec getProveedorid @id";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@id", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);
            result.Columns[0].ColumnName = "IDProveedor";
            return result;
        }

        //Obtiene el nombre del proveedor
        public DataTable getNombreProveedor(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec getNombreProveedor @id";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@id", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);
            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            return result;
        }
        //Modificacion del nombre del proveedor
        public int modificarProveedorTotal(int id, String nombre, String modi)
        {
            try
            {
                consulta = "exec modificarProveedorTotal @id,@nombre,@modi";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nombre", nombre);
                comando.Parameters.AddWithValue("@modi", modi);
                comando.Parameters.AddWithValue("@id", id);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        //Modificacion de estado del proveedor
        public int modificarProveedor(int id, int estado, String modi)
        {
            try
            {
                consulta = "exec modificarProveedor @id,@Estado,@modi";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@Estado", estado);
                comando.Parameters.AddWithValue("@modi", modi);
                comando.Parameters.AddWithValue("@id", id);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

//---------------------------------------------CLIENTE-----------------------------------------------------//

        //Insertar Cliente
        public int insertarCliente(String Nombre, String Tipo, String FechaCreacion,int aprob,int estado)
        {
            try
            {

                consulta = "exec insertarCliente @Nombre,@Tipo,@FechaCreacion,@Aprobacion,@Estado";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@Nombre", Nombre);
                comando.Parameters.AddWithValue("@Tipo", Tipo);
                comando.Parameters.AddWithValue("@Aprobacion", aprob);
                comando.Parameters.AddWithValue("@FechaCreacion", FechaCreacion);
                comando.Parameters.AddWithValue("@Estado", estado);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        //Editar Cliente por completo
        public int modificarCliente(int id, String nombres,int tipo, String FechaMod)
        {
            try
            {
                consulta = "exec modificarCliente @id,@nombre,@tipo,@FechaModi";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nombre", nombres);
                comando.Parameters.AddWithValue("@tipo", tipo);
                comando.Parameters.AddWithValue("@FechaModi", FechaMod);
                comando.Parameters.AddWithValue("@id", id);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        //Modificacion de estado del cliente
        public int modificarCliente(int id, int estado, String modi)
        {
            try
            {
                consulta = "exec modificarClienteEstado @id,@Estado,@modi";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@Estado", estado);
                comando.Parameters.AddWithValue("@modi", modi);
                comando.Parameters.AddWithValue("@id", id);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        //Obtener valores de Clientes de alta
        public DataTable getClientes(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec getClientes @id";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@id", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Tipo";
            result.Columns[3].ColumnName = "Estado";
            result.Columns[4].ColumnName = "Fecha Creacion";
            result.Columns[5].ColumnName = "Fecha Modificación";

            return result;
        }


        //Obtener valores de Clientes de alta
        public DataTable getClientes()
        {
            DataTable result = new DataTable();
            consulta = "exec getClientesAlta";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Tipo";
            result.Columns[3].ColumnName = "Estado";
            result.Columns[4].ColumnName = "Fecha Creacion";
            result.Columns[5].ColumnName = "Fecha Modificación";
            
            return result;
        }

        //Obtener valores de Clientes de baja
        public DataTable getClientes2()
        {
            DataTable result = new DataTable();
            consulta = "exec getClientes2";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Tipo";
            result.Columns[3].ColumnName = "Estado";
            result.Columns[4].ColumnName = "Fecha Creacion";
            result.Columns[5].ColumnName = "Fecha Modificación";

            return result;
        }

        //Busqueda de Cliente por Alta
        public DataTable BusquedaCliente(int tipo)
        {
            DataTable result = new DataTable();
            consulta = "exec BusquedaCliente @tipo";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@tipo", tipo);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Tipo";
            result.Columns[3].ColumnName = "Estado";
            result.Columns[4].ColumnName = "Fecha Creacion";
            result.Columns[5].ColumnName = "Fecha Modificación";

            return result;
        }
        //Busqueda de cliente por baja
        public DataTable BusquedaCliente2(int tipo)
        {
            DataTable result = new DataTable();
            consulta = "exec BusquedaCliente2 @tipo";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@tipo", tipo);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Tipo";
            result.Columns[3].ColumnName = "Estado";
            result.Columns[4].ColumnName = "Fecha Creacion";
            result.Columns[5].ColumnName = "Fecha Modificación";

            return result;
        }

//---------------------------------------------BODEGA------------------------------------------------------//
        //Insertar Bodega
        public int insertarBodega(String Nombre, String Direccion,String FechaCreacion)
        {
            try
            {

                consulta = "exec insertarBodega @Nombre,@Direccion,@FechaCreacion";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@Nombre", Nombre);
                comando.Parameters.AddWithValue("@Direccion", Direccion);
                comando.Parameters.AddWithValue("@FechaCreacion", FechaCreacion);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        //Obtener valores de Bodega
        public DataTable getBodega()
        {
            DataTable result = new DataTable();


            consulta = "exec getBodega";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Direccion";
            result.Columns[3].ColumnName = "FechaCrea";
            result.Columns[4].ColumnName = "FechaModi";

            return result;
        }


//---------------------------------------------PRODUCTO------------------------------------------------------//
        
        //Insertar Producto
        public int insertarProducto(String Nombre, String FechaCreacion,int Estado ,int Proveedor, float PrecioCompra, float PrecioVenta,int Cantidad,int aprob)
        {
            try
            {

                consulta = "exec insertarProducto @Nombre,@FechaCrea,@Estado,@IDProveedor,@PrecioCompra,@PrecioVenta,@Cantidad,@Aprobacion";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@Nombre", Nombre);
                comando.Parameters.AddWithValue("@FechaCrea", FechaCreacion);
                comando.Parameters.AddWithValue("@Estado", Estado);
                comando.Parameters.AddWithValue("@IDProveedor", Proveedor);
                comando.Parameters.AddWithValue("@PrecioCompra", PrecioCompra);
                comando.Parameters.AddWithValue("@PrecioVenta", PrecioVenta);
                comando.Parameters.AddWithValue("@Cantidad", Cantidad);
                comando.Parameters.AddWithValue("@Aprobacion", aprob);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }


        //Obtener todos los valores de productos
        public DataTable getProductos()
        {
            DataTable result = new DataTable();
            consulta = "exec getProductos";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Fecha Creacion";
            result.Columns[3].ColumnName = "FechaModi";
            result.Columns[4].ColumnName = "Estado";
            result.Columns[5].ColumnName = "IDProveedor";
            result.Columns[6].ColumnName = "PrecioCompra";
            result.Columns[7].ColumnName = "PrecioVenta";
            result.Columns[8].ColumnName = "Cantidad";
            return result;
        }

        //Obtener todos los valores de productos por alta
        public DataTable getProductosAlta()
        {
            DataTable result = new DataTable();
            consulta = "exec getProductosAlta";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Fecha Creacion";
            result.Columns[3].ColumnName = "FechaModi";
            result.Columns[4].ColumnName = "Estado";
            result.Columns[5].ColumnName = "IDProveedor";
            result.Columns[6].ColumnName = "PrecioCompra";
            result.Columns[7].ColumnName = "PrecioVenta";
            result.Columns[8].ColumnName = "Cantidad";
            return result;
        }

        //Obtener todos los valores de productos por baja
        public DataTable getProductosBaja()
        {
            DataTable result = new DataTable();
            consulta = "exec getProductosBaja";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Fecha Creacion";
            result.Columns[3].ColumnName = "FechaModi";
            result.Columns[4].ColumnName = "Estado";
            result.Columns[5].ColumnName = "IDProveedor";
            result.Columns[6].ColumnName = "PrecioCompra";
            result.Columns[7].ColumnName = "PrecioVenta";
            result.Columns[8].ColumnName = "Cantidad";
            return result;
        }

        //Obtener la cantidad del producto por su ID
        public DataTable getCantidadProducto(string ID)
        {
            DataTable result = new DataTable();
            consulta = "exec getCantidadProducto @ID";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@ID", ID);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            return result;
        }

        //Obtener productos por ID
        public DataTable getProductos(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec getProductosID @id";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@id", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Proveedor";
            result.Columns[3].ColumnName = "Precio de compra";
            result.Columns[4].ColumnName = "Precio de venta";
            result.Columns[5].ColumnName = "Estado";
            return result;
        }


        //Editar Productos por completo
        public int modificarProducto(int id, String nombres,int IDProveedor,float PrecioCompra, float PrecioVenta,int Cantidad, String FechaMod)
        {
            try
            {
                consulta = "exec modificarProducto @id,@nombre,@IDProveedor,@PrecioCompra,@PrecioVenta,@Cantidad,@FechaModi";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nombre", nombres);
                comando.Parameters.AddWithValue("@IDProveedor", IDProveedor);
                comando.Parameters.AddWithValue("@PrecioCompra", PrecioCompra);
                comando.Parameters.AddWithValue("@PrecioVenta", PrecioVenta);
                comando.Parameters.AddWithValue("@Cantidad", Cantidad);
                comando.Parameters.AddWithValue("@FechaModi", FechaMod);
                comando.Parameters.AddWithValue("@id", id);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }


        //Modificacion de estado del producto
        public int modificarProducto(int id, int estado, String modi)
        {
            try
            {
                consulta = "exec modificarProductoEstado @id,@Estado,@modi";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@Estado", estado);
                comando.Parameters.AddWithValue("@modi", modi);
                comando.Parameters.AddWithValue("@id", id);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        //Busqueda de producto por Alta
        public DataTable BusquedaProducto(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec BusquedaProducto @id";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@ID", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Fecha Creacion";
            result.Columns[3].ColumnName = "FechaModi";
            result.Columns[4].ColumnName = "Estado";
            result.Columns[5].ColumnName = "IDProveedor";
            result.Columns[6].ColumnName = "Precio de Compra";
            result.Columns[7].ColumnName = "Precio de Venta";
            result.Columns[8].ColumnName = "Cantidad";

            return result;
        }

        //Busqueda de producto por baja
        public DataTable BusquedaProducto2(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec BusquedaProducto2 @id";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@ID", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Nombre";
            result.Columns[2].ColumnName = "Fecha Creacion";
            result.Columns[3].ColumnName = "FechaModi";
            result.Columns[4].ColumnName = "Estado";
            result.Columns[5].ColumnName = "IDProveedor";
            result.Columns[6].ColumnName = "Precio de Compra";
            result.Columns[7].ColumnName = "Precio de Venta";
            result.Columns[8].ColumnName = "Cantidad";

            return result;
        }

//---------------------------------------------Inventario------------------------------------------------------//

        //Insertar nuevo movimiento por BodegaEntrada
        public int insertarMovimiento(String TMovimiento, int IDProducto, int IDUsuario, int BodegaEntrada, string FechaEntrada, int cantidad)
        {
            try
            {

                consulta = "exec insertarMovimiento @TMovimiento,@IDProducto,@IDUsuario,@BodegaEntrada,@FechaEntrada,@Cantidad";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@TMovimiento", TMovimiento);
                comando.Parameters.AddWithValue("@IDProducto", IDProducto);
                comando.Parameters.AddWithValue("@IDUsuario", IDUsuario);
                comando.Parameters.AddWithValue("@BodegaEntrada", BodegaEntrada);
                comando.Parameters.AddWithValue("@FechaEntrada", FechaEntrada);
                comando.Parameters.AddWithValue("@Cantidad", cantidad);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        //Insertar nuevo movimiento por BodegaSalida
        public int insertarMovimiento2(String TMovimiento, int IDProducto, int IDUsuario, int BodegaSalida, string FechaSalida, int cantidad)
        {
            try
            {

                consulta = "exec insertarMovimiento2 @TMovimiento,@IDProducto,@IDUsuario,@BodegaSalida,@FechaSalida,@Cantidad";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@TMovimiento", TMovimiento);
                comando.Parameters.AddWithValue("@IDProducto", IDProducto);
                comando.Parameters.AddWithValue("@IDUsuario", IDUsuario);
                comando.Parameters.AddWithValue("@BodegaSalida", BodegaSalida);
                comando.Parameters.AddWithValue("@FechaSalida", FechaSalida);
                comando.Parameters.AddWithValue("@Cantidad", cantidad);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        //Obtener todos los valores de inventario
        public DataTable getInventario()
        {
            DataTable result = new DataTable();
            consulta = "exec getInventario";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Tipo de Movimiento";
            result.Columns[2].ColumnName = "ID del producto";
            result.Columns[3].ColumnName = "ID del Usuario";
            result.Columns[4].ColumnName = "Bodega de Entrada";
            result.Columns[5].ColumnName = "Fecha de Entrada";
            result.Columns[6].ColumnName = "Bodega de Salida";
            result.Columns[7].ColumnName = "Fecha de Salida";
            result.Columns[8].ColumnName = "Cantidad";
            return result;
        }

        //Busqueda de producto
        public DataTable BusquedaInventario(string TMovimiento, int idProd , int bodega)
        {
            DataTable result = new DataTable();
            consulta = "exec BusquedaInventario @TMovimiento,@IDProducto,@IDBodega";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@TMovimiento", TMovimiento);
            comando.Parameters.AddWithValue("@IDProducto", idProd);
            comando.Parameters.AddWithValue("@IDBodega", bodega);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Tipo de Movimiento";
            result.Columns[2].ColumnName = "ID del producto";
            result.Columns[3].ColumnName = "ID del Usuario";
            result.Columns[4].ColumnName = "Bodega de Entrada";
            result.Columns[5].ColumnName = "Fecha de Entrada";
            result.Columns[6].ColumnName = "Bodega de Salida";
            result.Columns[7].ColumnName = "Fecha de Salida";
            result.Columns[8].ColumnName = "Cantidad";

            return result;
        }


//---------------------------------------------Usuarios------------------------------------------------------//
        //Insertar Bodega
        public int insertarUsuario(String Nombre,String contraseña, int TipoUsuario, String FechaCreacion, int estado)
        {
            try
            {

                consulta = "exec insertarUsuario @Nombre,@Contraseña,@IDTipoUsuario,@FechaCrea,@Estado";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@Nombre", Nombre);
                comando.Parameters.AddWithValue("@Contraseña", contraseña);
                comando.Parameters.AddWithValue("@IDTipoUsuario", TipoUsuario);
                comando.Parameters.AddWithValue("@FechaCrea", FechaCreacion);
                comando.Parameters.AddWithValue("@Estado", estado);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        //Obtener todos los valores de Usuarios por id
        public DataTable getUsuarios(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec getUsuarios @id";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@id", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "UserName";
            result.Columns[2].ColumnName = "Contraseña";
            result.Columns[3].ColumnName = "IDTipoUsuario";
            result.Columns[4].ColumnName = "Fecha Creacion";
            result.Columns[5].ColumnName = "FechaModi";
            result.Columns[6].ColumnName = "Estado";
            return result;
        }

        //Obtener todos los valores de productos por alta
        public DataTable getUsuariosAlta()
        {
            DataTable result = new DataTable();
            consulta = "exec getUsuariosAlta";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "UserName";
            result.Columns[2].ColumnName = "Contraseña";
            result.Columns[3].ColumnName = "IDTipoUsuario";
            result.Columns[4].ColumnName = "Fecha Creacion";
            result.Columns[5].ColumnName = "FechaModi";
            result.Columns[6].ColumnName = "Estado";
            return result;
        }

        //Busqueda de Usuarios por Alta
        public DataTable BusquedaUsuarios(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec BusquedaUsuarios @id";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@id", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "UserName";
            result.Columns[2].ColumnName = "Contraseña";
            result.Columns[3].ColumnName = "IDTipoUsuario";
            result.Columns[4].ColumnName = "Fecha Creacion";
            result.Columns[5].ColumnName = "FechaModi";
            result.Columns[6].ColumnName = "Estado";

            return result;
        }

        //Obtener todos los valores de productos por alta
        public DataTable getUsuariosBaja()
        {
            DataTable result = new DataTable();
            consulta = "exec getUsuariosBaja";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "UserName";
            result.Columns[2].ColumnName = "Contraseña";
            result.Columns[3].ColumnName = "IDTipoUsuario";
            result.Columns[4].ColumnName = "Fecha Creacion";
            result.Columns[5].ColumnName = "FechaModi";
            result.Columns[6].ColumnName = "Estado";
            return result;
        }

        //Busqueda de Usuarios por Alta
        public DataTable BusquedaUsuarios2(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec BusquedaUsuarios2 @Tipo";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@Tipo", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "UserName";
            result.Columns[2].ColumnName = "Contraseña";
            result.Columns[3].ColumnName = "IDTipoUsuario";
            result.Columns[4].ColumnName = "Fecha Creacion";
            result.Columns[5].ColumnName = "FechaModi";
            result.Columns[6].ColumnName = "Estado";

            return result;
        }

        //Modificacion de estado del producto completo
        public int modificarUsuario(int id, String UserName, String Contraseña, int idUser,String Modi)
        {
            try
            {
                consulta = "exec modificarUsuario @id,@user,@Contraseña,@IDUsuario,@Modi";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@user", UserName);
                comando.Parameters.AddWithValue("@Contraseña", Contraseña);
                comando.Parameters.AddWithValue("@IDUsuario", idUser);
                comando.Parameters.AddWithValue("@Modi", Modi);
                comando.Parameters.AddWithValue("@id", id);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        //Modificacion de estado del producto
        public int modificarUsuario(int id, int estado, String modi)
        {
            try
            {
                consulta = "exec modificarUsuarioEstado @id,@Estado,@modi";
                comando = new SqlCommand();
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@Estado", estado);
                comando.Parameters.AddWithValue("@modi", modi);
                comando.Parameters.AddWithValue("@id", id);
                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = consulta;
                comando.Connection = conexionSQL;

                return comando.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

//---------------------------------------------Tipos de usuarios------------------------------------------------------//

        //Obtiene los datos de tipos de usuarios
        public DataTable getTipoUsuario()
        {
            DataTable result = new DataTable();


            consulta = "exec getTipoUsuario";
            comando = new SqlCommand(consulta, conexionSQL);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);

            result.Columns[0].ColumnName = "ID";
            result.Columns[1].ColumnName = "Cargo";

            return result;
        }

        //Obtiene el nombre del proveedor
        public DataTable getCargoUsuario(int id)
        {
            DataTable result = new DataTable();
            consulta = "exec getCargoUsuario @id";
            comando = new SqlCommand(consulta, conexionSQL);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@id", id);
            adaptador = new SqlDataAdapter(comando);
            adaptador.Fill(result);
            result.Columns[0].ColumnName = "id";
            result.Columns[1].ColumnName = "Cargo";
            return result;
        }

    }
}