﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class OrdenarVenta : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmd2 = new SqlCommand();
        SqlCommand cmd3 = new SqlCommand();
        SqlCommand cmd4 = new SqlCommand();
        SqlCommand cmd5 = new SqlCommand();
        SqlCommand cmd6 = new SqlCommand();
        SqlCommand cmd7 = new SqlCommand();
        SqlCommand cmd8 = new SqlCommand();

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);

        int id = 0;
        int idProveedor = 0;
        float precioUnitario = 0;
        int UltimoID = 0;
        int cantidadMax = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            txtNombre.Enabled = false;
            txtPrecio.Enabled = false;
            id = int.Parse(Request.QueryString["id"]);//obtiene el id del producto a ordenar
            using (conn)
            {
                //Obtiene los datos de bodega
                cmd8.CommandType = CommandType.StoredProcedure;
                cmd8.CommandText = "getBodega";
                cmd8.Connection = conn;
                conn.Open();
                ddlBodega.DataSource = cmd8.ExecuteReader();
                ddlBodega.DataTextField = "Nombre";
                ddlBodega.DataValueField = "id";
                ddlBodega.DataBind();
                conn.Close();

                //Obtiene el producto por el id
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "getProductosID";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                cmd.Connection = conn;
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                txtNombre.Text = Convert.ToString(dr["Nombre"]).TrimEnd();
                txtPrecio.Text = Convert.ToString(dr["Precio de venta"]);
                precioUnitario = float.Parse(Convert.ToString(dr["Precio de venta"]));
                idProveedor = Int32.Parse(Convert.ToString(dr["Proveedor"]));
                cantidadMax = Int32.Parse(Convert.ToString(dr["Cantidad"]));
                conn.Close();
            }
            cantidadMaxima.Text = "La cantidad maxima posible de venta es: " + cantidadMax;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            int descontarCantidad = cantidadMax - Int32.Parse(txtCant.Text);
            if (Convert.ToInt32(txtCant.Text) <= cantidadMax)
            {   
                using (conn2)
                {
                    //Obtiene el ultimo id ingresado de la tabla ventas
                    cmd3.CommandText = "UltimoIDVenta";
                    cmd3.Connection = conn2;
                    conn2.Open();
                    SqlDataReader dr = cmd3.ExecuteReader();
                    dr.Read();
                    UltimoID = Int32.Parse(Convert.ToString(dr["Ultimo"]));
                    Session["ultimoVenta"] = Int32.Parse(Convert.ToString(dr["Ultimo"]));
                    conn2.Close();

                    //Actualiza la cantidad de productos
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.CommandText = "ActualizarCantidadVenta";
                    cmd2.Parameters.Add("@cantidad", SqlDbType.Int).Value = Convert.ToInt32(txtCant.Text);
                    cmd2.Parameters.Add("@id", SqlDbType.Int).Value = id;
                    cmd2.Connection = conn2;
                    conn2.Open();
                    cmd2.ExecuteNonQuery();
                    conn2.Close();

                    //Inserta un movimiento para salida de productos
                    String actual = DateTime.Now.ToString("yyyy-MM-dd");
                    cmd7.CommandType = CommandType.StoredProcedure;
                    cmd7.CommandText = "insertarMovimiento2";
                    cmd7.Parameters.Add("@TMovimiento", SqlDbType.NVarChar).Value = "S";
                    cmd7.Parameters.Add("@IDProducto", SqlDbType.Int).Value = id;
                    cmd7.Parameters.Add("@IDUsuario", SqlDbType.Int).Value = (int)Session["idCliente"];
                    cmd7.Parameters.Add("@BodegaSalida", SqlDbType.Int).Value = Int32.Parse(ddlBodega.SelectedItem.Value);
                    cmd7.Parameters.Add("@FechaSalida", SqlDbType.Date).Value = actual;
                    cmd7.Parameters.Add("@Cantidad", SqlDbType.Int).Value = Int32.Parse(txtCant.Text);
                    cmd7.Connection = conn2;
                    conn2.Open();
                    cmd7.ExecuteNonQuery();
                    conn2.Close();

                    //Inserta datos a la tabla detalle en donde van los productos y sus cantidades
                    cmd4.CommandType = CommandType.StoredProcedure;
                    cmd4.CommandText = "InsertarDetalleVenta";
                    cmd4.Parameters.Add("@idProducto", SqlDbType.Int).Value = id;
                    cmd4.Parameters.Add("@IDProveedor", SqlDbType.Int).Value = idProveedor;
                    cmd4.Parameters.Add("@Cantidad", SqlDbType.Int).Value = Convert.ToInt32(txtCant.Text);
                    cmd4.Parameters.Add("@PrecioUnitario", SqlDbType.Float).Value = Math.Round(precioUnitario, 2);
                    cmd4.Parameters.Add("@IDVenta", SqlDbType.Int).Value = UltimoID;
                    cmd4.Connection = conn2;
                    conn2.Open();
                    cmd4.ExecuteNonQuery();
                    conn2.Close();

                    cmd6.CommandType = CommandType.StoredProcedure;
                    cmd6.CommandText = "ActualizarCantidadProducto";
                    cmd6.Parameters.Add("@id", SqlDbType.Int).Value = id;
                    cmd6.Parameters.Add("@Cantidad", SqlDbType.Int).Value = descontarCantidad;
                   

                    cmd6.Connection = conn2;
                    conn2.Open();
                    cmd6.ExecuteNonQuery();
                    conn2.Close();

                    Session["cantidad2"] = Int32.Parse(txtCant.Text);
                }
                Response.Redirect("Venta.aspx?a=s");
            }
            else
            {
                lblerror.Text = "Existencias insuficientes para cumplir con el pedido";
            }

        }
    }
}