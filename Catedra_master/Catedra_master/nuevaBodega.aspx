﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="nuevaBodega.aspx.cs" Inherits="Catedra_master.nuevaBodega" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
    <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li><a href="listInventario.aspx" class="button
special">Listado de movimientos</a></li>
 </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
     <h3>Añadir Bodega</h3>
 <p>
 Rellene los siguientes campos
 </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row uniform 50%">
 <div class="6u 12u$(xsmall)">
 <label>Nombre</label>
 <asp:TextBox ID="txtNombre"
 runat="server"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator1"
ControlToValidate="txtNombre" ForeColor="Red"
 runat="server" ErrorMessage="Nombre obligatorio"></asp:RequiredFieldValidator>
     <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato Incorrecto. Ingrese de nuevo" ControlToValidate="txtNombre" ValidationExpression="^([a-zA-Z]*)(\s|\-)?([a-zA-Z]*)$" ForeColor="#ff490d"></asp:RegularExpressionValidator>

 </div>
      <div class="6u 12u$(xsmall)">
 <label>Dirección</label>
 <asp:TextBox ID="txtDireccion"
 runat="server"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator5"
ControlToValidate="txtDireccion" ForeColor="Red"
 runat="server" ErrorMessage="Dirección obligatoria"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator  runat="server"  ControlToValidate="txtNombre"
            ErrorMessage="Formato de entrada incorrecto" ValidationExpression="^([ñA-Za-zñÑáéíóúÁÉÍÓÚ ]*[ñA-Za-zñÑáéíóúÁÉÍÓÚ][ñA-Za-zñÑáéíóúÁÉÍÓÚ ])" ForeColor="#ff490d"></asp:RegularExpressionValidator>
 </div>
      <div class="6u 12u$(xsmall)">
 <asp:Button ID="btnBuscar" runat="server"
 Text="Registrar bodega" CssClass="special"
OnClick="btnBuscar_Click" />
 </div>
 </div>
</asp:Content>
