﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class listProveedor : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }

            try
            {
                //Validando que se haya iniciado sesión
                //Escondiendo todas las opciones
                nuevoProveedor.Visible = false;
                listProveedorBaja.Visible = false;
                NuevaSolicitudProveedor.Visible = false;
                //Obteniendo las variables de sesión
                string idRol = Session["TipoUsuario"].ToString();
                //Mostrando las opciones acorde al rol
                if (idRol == "1")//Administradores
                {
                    nuevoProveedor.Visible = true;
                    listProveedorBaja.Visible = true;
                    //Cargando los datos de proveedor al gridview
                    if (IsPostBack == false)
                    {
                        using (conn)
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getProveedor";
                            cmd.Connection = conn;
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getProveedorSolicitud";
                            cmd.Connection = conn;
                            conn.Open();
                            GridView2.DataSource = cmd.ExecuteReader();
                            GridView2.DataBind();
                            conn.Close();
                        }
                    }
                }
                else if (idRol == "4")//Jefes de Relaciones Exteriores
                {
                    nuevoProveedor.Visible = true;
                    listProveedorBaja.Visible = true;
                    //Cargando los datos de proveedor al gridview
                    if (IsPostBack == false)
                    {
                        using (conn)
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getProveedor";
                            cmd.Connection = conn;
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getProveedorSolicitud";
                            cmd.Connection = conn;
                            conn.Open();
                            GridView2.DataSource = cmd.ExecuteReader();
                            GridView2.DataBind();
                            conn.Close();
                        }
                    }
                }
                else if (idRol == "8")//Usuarios de relaciones exteriores
                {
                    NuevaSolicitudProveedor.Visible = true;
                    titulosolicitud.Visible = false;
                    GridView1.Columns[1].Visible = false;
                    //Cargando los datos de proveedor al gridview
                    if (IsPostBack == false)
                    {
                        using (conn)
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "getProveedor";
                            cmd.Connection = conn;
                            conn.Open();
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                            conn.Close();
                        }
                    }
                }
                }
            catch (Exception error)
            {
                Response.Redirect("menu.aspx");
            }





           
            
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Obtiene la fecha actual y da de baja al proveedor
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");
            try
            {
                //Dando de baja al proveedor seleccionado y actualizando el GridView
                id = int.Parse(GridView1.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarProveedor";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 2;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {

            }
            Response.Redirect("listProveedor.aspx");
        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de proveedores, obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("EditarProveedor.aspx?id={0}", nIdEmpleado));
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Obtiene la fecha actual y da de baja al proveedor
            int id = 0;
            String actual = DateTime.Now.ToString("yyyy-MM-dd");
            try
            {
                //Dando de baja al proveedor seleccionado y actualizando el GridView
                id = int.Parse(GridView2.DataKeys[e.RowIndex].Values["ID"].ToString());
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "modificarProveedor";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 2;
                    cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = actual;
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {

            }
            Response.Redirect("listProveedor.aspx");
        }

        protected void GridView2_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Modificacion de proveedores, obtiene el id
            int nIdEmpleado = Convert.ToInt32(GridView2.DataKeys[e.NewSelectedIndex].Value);
            Response.Redirect(string.Format("AprobarSolicitudProveedor.aspx?id={0}", nIdEmpleado));
        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}