﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class nuevoProveedor : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //inserta nuevo proveedor y obtiene la fecha actual
            String actual = DateTime.Now.ToString("yyyy-MM-dd");

            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "insertarProveedor";
                cmd.Parameters.Add("@Nombre", SqlDbType.NVarChar).Value = txtNombre.Text;
                cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@FechaCreacion", SqlDbType.Date).Value = actual;
                cmd.Parameters.Add("@Aprobacion", SqlDbType.Int).Value = 1;
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            Response.Redirect("listProveedor.aspx");
        }
    }
}