﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Compra.aspx.cs" Inherits="Catedra_master.Compra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
   <header id="header" class="skel-layers-fixed">
       <style>
           .Horizontal {
    margin-top: 5%;
    margin-left: -17%;
}
       </style>
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li><a href="VerCarrito.aspx" class="button
special">Ver Carrito</a></li>
     <li><a href="verSolicitud.aspx" class="button
special">Ver Solicitudes</a></li>
      </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="titulo" runat="server">
    <header class="major">
 <h2>Compra de producto</h2>
 <p>FÉNIX Alimentos</p>
 </header>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row uniform 50%">
       
        <div class="2u 12u$(xsmall)">
             <br />
             <asp:Button ID="BtnLimpiar" runat="server"
 Text="Nueva compra" Width="100%" CssClass="special" OnClick="BtnLimpiar_Click"/>
             </div>
         <br />
        <div>
            </div>
            <br />
        <div class="Horizontal">
        <div class="row uniform 50%">
           
<asp:GridView ID="GridView1" runat="server" 
    DataKeyNames="ID,Nombre,IDProveedor"
    OnSelectedIndexChanging="GridView1_SelectedIndexChanging">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/carrito.png" HeaderText="Añadir" ShowSelectButton="True" SelectImageUrl="~/images/carrito.png">
            <controlstyle width="50px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>
                </div>
            </div>
</asp:Content>
