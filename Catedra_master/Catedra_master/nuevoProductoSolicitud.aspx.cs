﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class nuevoProductoSolicitud : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);

        Conexion miConexion = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            if (IsPostBack == false)
            {
                //Cargar Datos al dropdownlist de proveedor
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getProveedor";
                    cmd.Connection = conn;
                    conn.Open();
                    ddlProveedor.DataSource = cmd.ExecuteReader();
                    ddlProveedor.DataTextField = "Nombre";
                    ddlProveedor.DataValueField = "id";
                    ddlProveedor.DataBind();
                    conn.Close();
                }
            }

        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            // Validar precio de venta
            if (float.Parse(txtPrecioVenta.Text) > float.Parse(txtPrecioCompra.Text))
            {
                //obtiene fecha actual e inserta datos del producto
                String actual = DateTime.Now.ToString("yyyy-MM-dd");
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertarProducto";
                    cmd.Parameters.Add("@Nombre", SqlDbType.NVarChar).Value = txtNombre.Text;
                    cmd.Parameters.Add("@FechaCrea", SqlDbType.Date).Value = actual;
                    cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@IDProveedor", SqlDbType.Int).Value = Int32.Parse(ddlProveedor.SelectedItem.Value);
                    cmd.Parameters.Add("@PrecioCompra", SqlDbType.Decimal).Value = float.Parse(txtPrecioCompra.Text);
                    cmd.Parameters.Add("@PrecioVenta", SqlDbType.Decimal).Value = float.Parse(txtPrecioVenta.Text);
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Int).Value = Int32.Parse(txtCant.Text);
                    cmd.Parameters.Add("@Aprobacion", SqlDbType.Int).Value = 0;

                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                Response.Redirect("listProducto.aspx");
            }
            else
            {
                lblerror.Text = "El precio de venta a ingresar debe ser mayor al precio de compra para obtener ganancias.";
            }
        }
    }
}