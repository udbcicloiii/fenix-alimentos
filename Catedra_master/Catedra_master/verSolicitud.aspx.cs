﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class verSolicitud : System.Web.UI.Page
    {
        //Instancias con la bdd
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmd2 = new SqlCommand();
        SqlCommand cmd3 = new SqlCommand();
        SqlCommand cmd4 = new SqlCommand();
        SqlCommand cmd5 = new SqlCommand();
        SqlCommand cmd6 = new SqlCommand();
        SqlCommand cmd7 = new SqlCommand();
        SqlCommand cmd8 = new SqlCommand();
        SqlCommand cmd9 = new SqlCommand();

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn3 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn4 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn5 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn6 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        SqlConnection conn7 = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }

            using (conn)
            {
                //Obtiene la lista de compras a aprobar
                cmd.CommandText = "getListaComprasAprob";
                cmd.Connection = conn;
                conn.Open();
                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
                conn.Close();

                //Obtiene la lista compras rechazadas
                cmd4.CommandType = CommandType.StoredProcedure;
                cmd4.CommandText = "getListaComprasDesAprob";
                cmd4.Connection = conn;
                conn.Open();
                GridView2.DataSource = cmd4.ExecuteReader();
                GridView2.DataBind();
                conn.Close();

                //Obtiene la lista de ventas a aprobar
                cmd5.CommandType = CommandType.StoredProcedure;
                cmd5.CommandText = "getListaVentasAprob";
                cmd5.Connection = conn;
                conn.Open();
                GridView3.DataSource = cmd5.ExecuteReader();
                GridView3.DataBind();
                conn.Close();

                //Obtiene la lista ventas rechazadas
                cmd6.CommandType = CommandType.StoredProcedure;
                cmd6.CommandText = "getListaVentasDesAprob";
                cmd6.Connection = conn;
                conn.Open();
                GridView4.DataSource = cmd6.ExecuteReader();
                GridView4.DataBind();
                conn.Close();
            }
        }
        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            
            //Instanciando el servicio para añadir la consulta
            ServiceFenix.WebServiceFenixSoapClient servicio = new ServiceFenix.WebServiceFenixSoapClient();
            Boolean ingresar;//variable para verificar si se ha ingresado correctamente
            string mensaje;//mensaje
                           //idUser = "DR2";
                           //Ingresando la consulta utilizando el webservice
            ingresar = servicio.ingresarFactura(0, (int)Session["id"], Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Values["IDProveedor"].ToString()), "E");
            //verificacion de ingreso.
            if (ingresar)//Si se ha ingresado correctamente se redirije
            {
                using (conn4)
                {

                    cmd6.CommandType = CommandType.StoredProcedure;
                    cmd6.CommandText = "AsignarFacturaDetalle"; 
                    cmd6.Parameters.Add("@IDCompra", SqlDbType.Int).Value = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Values["id"].ToString());
                    cmd6.Connection = conn4;
                    conn4.Open();
                    cmd6.ExecuteNonQuery();
                    conn4.Close();

                }

                using (conn6)
                {
                    cmd8.CommandType = CommandType.StoredProcedure;
                    cmd8.CommandText = "ActualizarEstadoCompraRetirar";
                    cmd8.Parameters.Add("@aprob", SqlDbType.Int).Value = 3;
                    cmd8.Parameters.Add("@id", SqlDbType.Int).Value = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Values["id"].ToString());
                    cmd8.Connection = conn6;
                    conn6.Open();
                    cmd8.ExecuteNonQuery();
                    conn6.Close();

                }

                mensaje = "Consulta agregada exitosamente";
                int nIdSolicitud = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Values["IDComprador"].ToString());
                Response.Redirect(string.Format("facturacion.aspx?idcomprador={0}", nIdSolicitud));
            }
            else//Si no se muestra un error
            {
                mensaje = "Error al ingresar la consulta";
            }
            Response.Write("<script type='text/javascript'>// <![CDATA[alert(\"" + mensaje +
           "\")//]]></ script > ");
 
            
               
        }

        protected void GridView3_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Instanciando el servicio para añadir la consulta
            ServiceFenix.WebServiceFenixSoapClient servicio2 = new ServiceFenix.WebServiceFenixSoapClient();
            Boolean ingresar2;//variable para verificar si se ha ingresado correctamente
            string mensaje;//mensaje
                           //Ingresando la consulta utilizando el webservice
            ingresar2 = servicio2.ingresarFactura(Convert.ToInt32(GridView3.DataKeys[e.NewSelectedIndex].Values["IDCliente"].ToString()), 0, Convert.ToInt32(GridView3.DataKeys[e.NewSelectedIndex].Values["IDProveedor"].ToString()), "S");
            //verificacion de ingreso.
            if (ingresar2)//Si se ha ingresado correctamente se redirije
            {
                using (conn5)
                {

                    cmd7.CommandType = CommandType.StoredProcedure;
                    cmd7.CommandText = "AsignarFacturaDetalleCliente";
                    cmd7.Parameters.Add("@IDVenta", SqlDbType.Int).Value = Convert.ToInt32(GridView3.DataKeys[e.NewSelectedIndex].Values["id"].ToString());
                    cmd7.Connection = conn5;
                    conn5.Open();
                    cmd7.ExecuteNonQuery();
                    conn5.Close();
                }

                using (conn7)
                {
                    cmd9.CommandType = CommandType.StoredProcedure;
                    cmd9.CommandText = "ActualizarEstadoVentaRetirar";
                    cmd9.Parameters.Add("@aprob", SqlDbType.Int).Value = 3;
                    cmd9.Parameters.Add("@id", SqlDbType.Int).Value = Convert.ToInt32(GridView3.DataKeys[e.NewSelectedIndex].Values["id"].ToString());
                    cmd9.Connection = conn7;
                    conn7.Open();
                    cmd9.ExecuteNonQuery();
                    conn7.Close();
                }

                mensaje = "Consulta agregada exitosamente";
                int nIdSolicitud2 = Convert.ToInt32(GridView3.DataKeys[e.NewSelectedIndex].Values["IDCliente"].ToString());
                Response.Redirect(string.Format("facturacion.aspx?idcliente={0}", nIdSolicitud2));
            }
            else//Si no se muestra un error
            {
                mensaje = "Error al ingresar la consulta";
            }
            Response.Write("<script type='text/javascript'>// <![CDATA[alert(\"" + mensaje +
           "\")//]]></ script > ");
        }

        protected void GridView3_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Rechaza solicitudes de ventas
            try
            {
                int id = int.Parse(GridView3.DataKeys[e.RowIndex].Values["id"].ToString());
                using (conn2)
                {
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.CommandText = "ActualizarEstadoVentaRetirar";
                    cmd2.Parameters.Add("@aprob", SqlDbType.Int).Value = 2;
                    cmd2.Parameters.Add("@id", SqlDbType.Int).Value = id;
                    cmd2.Connection = conn2;
                    conn2.Open();
                    cmd2.ExecuteNonQuery();
                    conn2.Close();

                    Response.Redirect("verSolicitud.aspx");
                }
            }
            catch (SqlException ex)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert(" + ex + ")</script>");
            }
        }
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Rechaza las solicitudes de compras
            try
            {
                int id = int.Parse(GridView1.DataKeys[e.RowIndex].Values["id"].ToString());
                using (conn2)
                {
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.CommandText = "ActualizarEstadoCompraRetirar";
                    cmd2.Parameters.Add("@aprob", SqlDbType.Int).Value = 2;
                    cmd2.Parameters.Add("@id", SqlDbType.Int).Value = id;
                    cmd2.Connection = conn2;
                    conn2.Open();
                    cmd2.ExecuteNonQuery();
                    conn2.Close();

                    Response.Redirect("verSolicitud.aspx");
                }
            }
            catch (SqlException ex)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert(" + ex + ")</script>");
            }
        }
    }
}