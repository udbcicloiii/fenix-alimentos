﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class menu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
           // System.Diagnostics.Debug.WriteLine("tipo de usuario " + Session["Tipo"]);

            try
            { 
                //Validando que se haya iniciado sesión
                //Escondiendo todas las opciones
                listUsuario.Visible = false;
                listProducto.Visible = false;
                listInventario.Visible = false;
                listCliente.Visible = false;
                listProveedor.Visible = false;
                listaSolicitudes.Visible = false;
                Venta.Visible = false;
                FacturasTodas.Visible = false;
                verSolicitud.Visible = false;
                //Obteniendo las variables de sesión

                string idRol = Session["TipoUsuario"].ToString();

                //Mostrando las opciones acorde al rol
                if (idRol == "1")//Administradores
                {
                    listUsuario.Visible = true;
                    listProducto.Visible = true;
                    listInventario.Visible = true;
                    listCliente.Visible = true;
                    listProveedor.Visible = true;
                    listaSolicitudes.Visible = true;
                    FacturasTodas.Visible = true;

                }
                else if (idRol == "2")//Jefes de Inventario
                {
                    listProducto.Visible = true;
                    listInventario.Visible = true;
                }
                else if (idRol == "3")//Jefes de compras
                {
                    listaSolicitudes.Visible = true;
                }
                else if (idRol == "4")//Jefes de Relaciones Exteriores
                {
                    listCliente.Visible = true;
                    listProveedor.Visible = true;
                }
                else if (idRol == "5")//Jefes de Facturación
                {
                    FacturasTodas.Visible = true;
                }
                else if (idRol == "6")//Usuarios de Inventario
                {
                    listProducto.Visible = true;
                    listInventario.Visible = true;

                }
                else if (idRol == "7")//Usuarios de compras
                {
                    listaSolicitudes.Visible = true;
                }
                else if (idRol == "8")//Usuarios de relaciones exteriores
                {
                    listCliente.Visible = true;
                    listProveedor.Visible = true;
                }
                else if (idRol == "9")//Usuarios de facturación
                {
                    verSolicitud.Visible= true;
                }
                else if (idRol == "10")//Cliente
                {
                    Venta.Visible = true;
                }
            }
            catch (Exception error)
            {
                Response.Redirect("Default.aspx");
            }


        }
    }
}