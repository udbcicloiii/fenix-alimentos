﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="VerCarrito.aspx.cs" Inherits="Catedra_master.VerCarrito" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
    <style>
        .msg {
            width: 100%;
            border: 1px solid;
            padding: 10px;
            margin: 20px;
            color: grey;
        }
        .msg-info {
            border-color: #020202;
            background-color: #343A3B;
            color: white;
        }
        .Label12{
            margin-left: 40%;
            font-size: 1.3em;
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        }
}
    </style>
 <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
     <li><a href="Compra.aspx" class="button
special">Compras</a></li>
      </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
    <header class="major">
 <h2>Lista de Compras</h2>
 <p>FÉNIX Alimentos</p>
 </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row uniform 50%">
     
<asp:GridView ID="GridView1" runat="server" 
    DataKeyNames="ID,IDProveedor"
    OnRowDeleting="GridView1_RowDeleting" OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
    <Columns>
        <asp:CommandField ButtonType="Image" EditImageUrl="~/images/eliminar_icon_flat.png" HeaderText="Eliminar" ShowDeleteButton="True" DeleteImageUrl="~/images/eliminar_icon_flat.png" SelectImageUrl="~/images/eliminar_icon_flat.png">
            <controlstyle width="40px" />
        </asp:CommandField>
    </Columns>
     </asp:GridView>

        <div class="msg msg-info z-depth-3">
            <asp:Label ID="Label1" cssClass="Label12" runat="server" Text="Label"></asp:Label>
        </div>
         <br />
         <div class="6u 12u$(xsmall)">
 <asp:Button ID="btnBuscar" runat="server"
 Text="Solicitar Compra" CssClass="special" OnClick="btnBuscar_Click" />
 </div>
 </div>
</asp:Content>
