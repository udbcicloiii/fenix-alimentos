﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class nuevoMovimiento : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
       
        String indice = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            if (IsPostBack == false)
            {
                //Cargar Datos A combobox de bodega
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getBodega";
                    cmd.Connection = conn;
                    conn.Open();
                    ddlBodega.DataSource = cmd.ExecuteReader();
                    ddlBodega.DataTextField = "Nombre";
                    ddlBodega.DataValueField = "id";
                    ddlBodega.DataBind();
                    conn.Close();

                    cmd.CommandText = "getProductos";
                    cmd.Connection = conn;
                    conn.Open();
                    ddlProducto.DataSource = cmd.ExecuteReader();
                    ddlProducto.DataTextField = "Nombre";
                    ddlProducto.DataValueField = "id";
                    ddlProducto.DataBind();
                    conn.Close();
                    if (ddlProducto.Items.Count == 0)
                    {
                        lblResultado.Text = "No puede crear un movimiento ya que no existen productos";
                        btnBuscar.Enabled = false;
                        
                        }
                    else
                    {

                    

                    if (indice == null || indice == "")//Cargando datos de cantidad del producto
                    {

                        cmd.CommandText = "getCantidadProducto";
                        cmd.Parameters.Add("@ID", SqlDbType.Int).Value = 1;
                        cmd.Connection = conn;
                        conn.Open();
                        SqlDataReader dr = cmd.ExecuteReader();
                        dr.Read();
                        TextBox1.Text = Convert.ToString(dr["Cantidad"]);
                        conn.Close();
                    }
                    else
                    {
                        cmd.CommandText = "getCantidadProducto";
                        cmd.Parameters.Add("@ID", SqlDbType.Int).Value = Int32.Parse(indice);
                        cmd.Connection = conn;
                        conn.Open();
                        SqlDataReader dr = cmd.ExecuteReader();
                        dr.Read();
                        TextBox1.Text = Convert.ToString(dr["Cantidad"]);
                        conn.Close();
                    }

                        if (ddlBodega.Items.Count == 0)
                        {
                            lblResultado.Text = "No puede crear un movimiento ya que no existen bodegas";
                            btnBuscar.Enabled = false;
                        }
                    }
                    
                }

            }
            
        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Obtine fecha actual
            String actual = DateTime.Now.ToString("yyyy-MM-dd");
            //Si el tipo de movimiento es de entrada inserta datos en bodega entrada y fecha entrada
            //si es de salida insertara datos en bodega salida y fecha salida
            if (ddlTipoMovimiento.SelectedItem.Value == "E")
            {
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertarMovimiento";
                    cmd.Parameters.Add("@TMovimiento", SqlDbType.NVarChar).Value = ddlTipoMovimiento.SelectedItem.Value;
                    cmd.Parameters.Add("@IDProducto", SqlDbType.Int).Value = Int32.Parse(ddlProducto.SelectedItem.Value);
                    cmd.Parameters.Add("@IDUsuario", SqlDbType.Int).Value = (int)Session["id"];
                    cmd.Parameters.Add("@BodegaEntrada", SqlDbType.Int).Value = Int32.Parse(ddlBodega.SelectedItem.Value);
                    cmd.Parameters.Add("@FechaEntrada", SqlDbType.Date).Value = actual;
                    cmd.Parameters.Add("@Cantidad", SqlDbType.Int).Value = Int32.Parse(txtCant.Text);
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                Response.Redirect("listInventario.aspx");
            }
            else
            {
                if(Int32.Parse(txtCant.Text) <= Int32.Parse(TextBox1.Text))//Verifica que la cantidad solicitada no sea mayor a la cantidad e inventario
                {
                    using (conn)
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "insertarMovimiento2";
                        cmd.Parameters.Add("@TMovimiento", SqlDbType.NVarChar).Value = ddlTipoMovimiento.SelectedItem.Value;
                        cmd.Parameters.Add("@IDProducto", SqlDbType.Int).Value = Int32.Parse(ddlProducto.SelectedItem.Value);
                        cmd.Parameters.Add("@IDUsuario", SqlDbType.Int).Value = (int)Session["id"];
                        cmd.Parameters.Add("@BodegaSalida", SqlDbType.Int).Value = Int32.Parse(ddlBodega.SelectedItem.Value);
                        cmd.Parameters.Add("@FechaSalida", SqlDbType.Date).Value = actual;
                        cmd.Parameters.Add("@Cantidad", SqlDbType.Int).Value = Int32.Parse(txtCant.Text);
                        cmd.Connection = conn;
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                    Response.Redirect("listInventario.aspx");
                }
                else
                {
                    Label1.Text = "La cantidad para salida del producto es mayor a la cantidad de inventario";
                }
                
            }

        }

        protected void ddlProducto_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Obtiene la cantidad del producto cuando cambia el indice del dropdownlist
            indice = (ddlProducto.SelectedItem.Value);
            if (IsPostBack == true)
            {
                using (conn)
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getCantidadProducto";
                    cmd.Parameters.Add("@ID", SqlDbType.Int).Value = Int32.Parse(indice);
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dr.Read();
                    TextBox1.Text = Convert.ToString(dr["Cantidad"]);
                    conn.Close();
                }
                
            }
        }
    }
}