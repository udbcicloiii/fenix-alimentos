﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Catedra_master
{
    public partial class Compra : System.Web.UI.Page
    {
        //Instancia con la bdd
        SqlCommand cmd = new SqlCommand();
        string a;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Verificar si se ha logueado
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            if (IsPostBack == false)
            {
                //obtiene los productos dados de alta en el gridview
                using (conn)
                {
                    //Obtiene los proveedores y los almacena en un dropdownlist
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conn;

                    //obtiene los productos
                    cmd.CommandText = "getProductosCompras";
                    conn.Open();
                    GridView1.DataSource = cmd.ExecuteReader();
                    GridView1.DataBind();
                    conn.Close();

                }
                a = (Request.QueryString["a"]);
                if (a == "s")
                {
                    BtnLimpiar.Enabled = false;
                }
                else
                {
                    //Bloquea los botones de compras
                    foreach (GridViewRow row in GridView1.Rows)
                    {
                        DataControlFieldCell editable = (DataControlFieldCell)row.Controls[0];
                        editable.Enabled = false;
                    }
                }

            }
        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //Se obtiene el id de proveedor
            int idProducto = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Value);
            Session["proveedor"] = Convert.ToInt32(GridView1.DataKeys[e.NewSelectedIndex].Values["IDProveedor"].ToString());
           
            Response.Redirect(string.Format("OrdenarCompra.aspx?id={0}", idProducto));

        }

        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            BtnLimpiar.Enabled = false;
            foreach (GridViewRow row in GridView1.Rows)
            {
                DataControlFieldCell editable = (DataControlFieldCell)row.Controls[0];
                editable.Enabled = true;
            }
            //Insertar una nueva compra
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "InsertarCompra";
                cmd.Parameters.Add("@Aprobacion", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@IDComprador", SqlDbType.Int).Value = Session["id"];
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

        }
    }
}