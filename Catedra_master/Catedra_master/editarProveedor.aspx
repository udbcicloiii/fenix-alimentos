﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="editarProveedor.aspx.cs" Inherits="Catedra_master.editarProveedor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
     <!-- Header -->
    <header id="header" class="skel-layers-fixed">
 <h1 id="logo"><a href="menu.aspx">Página Principal</a></h1>
 <nav id="nav">
 <ul>
 <li><a href="listCliente.aspx" class="button
special">Listado de clientes</a></li>
 </ul>
 </nav>
 </header>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo" runat="server">
    <h3>Editar Clientes</h3>
 <p>
 Rellene los siguientes campos
 </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row uniform 50%">
 <div class="6u 12u$(xsmall)">
 <label>Nombre</label>
 <asp:TextBox ID="txtNombre"
 runat="server"></asp:TextBox>
 <asp:RequiredFieldValidator
 ID="RequiredFieldValidator1"
ControlToValidate="txtNombre" ForeColor="Red"
 runat="server" ErrorMessage="Nombre obligatorio"></asp:RequiredFieldValidator>
     <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato Incorrecto. Ingrese de nuevo" ControlToValidate="txtNombre" ValidationExpression="^([ñA-Za-zñÑáéíóúÁÉÍÓÚ _]+[S.A ]+[de ]+[C.V.]+)|([ñA-Za-zñÑáéíóúÁÉÍÓÚ _]*[ñA-Za-zñÑáéíóúÁÉÍÓÚ][ñA-Za-zñÑáéíóúÁÉÍÓÚ _])" ForeColor="#ff490d"></asp:RegularExpressionValidator>

 </div>
         <br />
      <div class="6u 12u$(xsmall)">
         

 <asp:Button ID="btnBuscar" runat="server"
 Text="Registrar Proveedor" CssClass="special"
OnClick="btnBuscar_Click" />
 </div>
 </div>
</asp:Content>
