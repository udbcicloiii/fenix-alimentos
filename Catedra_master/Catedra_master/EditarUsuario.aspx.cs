﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Catedra_master
{
    public partial class EditarUsuario : System.Web.UI.Page
    {
        Conexion miConexion = new Conexion();
        int id = 0;

        SqlCommand cmd = new SqlCommand();
        SqlCommand cmd2 = new SqlCommand();
        SqlCommand cmd3 = new SqlCommand();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validando si se ha iniciado sesión
            if (!(Session["user"] != null))
            {
                Response.Redirect("Default.aspx");
            }
            ddlTipoUsuario.Enabled = false;//Se desabilita en dropdownlist
            id = int.Parse(Request.QueryString["id"]);//obtiene el id del usuario a editar
            int prov;

            if (!Page.IsPostBack)
            {
                //Rellena datos en el dropdownlist con la lista completa de cargos
                using (conn)
                {
                    //Datos del usuario para los textbox
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getUsuarios";
                    cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dr.Read();
                    txtUsuario.Text = Convert.ToString(dr["UserName"]).TrimEnd();
                    txtContraseña.Text = Convert.ToString(dr["Contraseña"]).TrimEnd();
                    prov = Convert.ToInt32(dr["IDTipoUsuario"]);
                    conn.Close();

                    //Obtener cargo actual
                    cmd3.CommandType = CommandType.StoredProcedure;
                    cmd3.CommandText = "getCargoUsuario";
                    cmd3.Parameters.Add("@id", SqlDbType.Int).Value = prov;
                    cmd3.Connection = conn;
                    conn.Open();
                    ddlTipoUsuario.DataSource = cmd3.ExecuteReader();
                    ddlTipoUsuario.DataTextField = "Cargo";
                    ddlTipoUsuario.DataBind();
                    conn.Close();

                    //Mostrar todos los cargos
                    cmd2.CommandText = "getTipoUsuario";
                    cmd2.Connection = conn;
                    conn.Open();
                    DropDownList1.DataSource = cmd2.ExecuteReader();
                    DropDownList1.DataTextField = "Cargo";
                    DropDownList1.DataValueField = "ID";
                    DropDownList1.DataBind();
                    conn.Close();
                }           

            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Obtiene la fecha actual y modifica el usuario
            String modificar = DateTime.Now.ToString("yyyy-MM-dd");
            using (conn)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "modificarUsuario";
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@user", SqlDbType.NVarChar).Value = txtUsuario.Text;
                cmd.Parameters.Add("@Contraseña", SqlDbType.NVarChar).Value = txtContraseña.Text;
                cmd.Parameters.Add("@IDUsuario", SqlDbType.Int).Value = Int32.Parse(DropDownList1.SelectedItem.Value);
                cmd.Parameters.Add("@Modi", SqlDbType.Date).Value = modificar;
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            Response.Redirect("listUsuario.aspx");
        }
    }
}