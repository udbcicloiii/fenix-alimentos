USE [master]
GO
CREATE DATABASE [Fenix_Alimentos]
GO
ALTER DATABASE [Fenix_Alimentos] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET ARITHABORT OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Fenix_Alimentos] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Fenix_Alimentos] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Fenix_Alimentos] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Fenix_Alimentos] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Fenix_Alimentos] SET  MULTI_USER 
GO
ALTER DATABASE [Fenix_Alimentos] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Fenix_Alimentos] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Fenix_Alimentos] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Fenix_Alimentos] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Fenix_Alimentos] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Fenix_Alimentos] SET QUERY_STORE = OFF
GO
USE [Fenix_Alimentos]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [Fenix_Alimentos]
GO
/****** Object:  Table [dbo].[Bodega]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bodega](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Direccion] [nvarchar](80) NOT NULL,
	[FechaCrea] [date] NULL,
	[FechaModi] [date] NULL,
 CONSTRAINT [PK_Bodega] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Tipo] [int] NOT NULL,
	[FechaCrea] [date] NOT NULL,
	[FechaModi] [date] NULL,
	[Aprobacion] [int] NULL,
	[Estado] [int] NOT NULL,
	[Contraseña] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Compra]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Compra](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MontoTotal] [float] NULL,
	[Aprobacion] [int] NULL,
	[IDComprador] [int] NOT NULL,
	[IDProveedor] [int] NULL,
 CONSTRAINT [PK_Compra] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleCompraVenta]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleCompraVenta](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDProducto] [int] NOT NULL,
	[IDProveedor] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[PrecioUnitario] [float] NOT NULL,
	[IDCompra] [int] NULL,
	[IDVenta] [int] NULL,
	[IDFactura] [int] NULL,
 CONSTRAINT [PK_DetalleCompraVenta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Factura]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Factura](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDComprador] [int] NOT NULL,
	[IDProveedor] [int] NOT NULL,
	[TMovimiento] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Factura] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventario]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventario](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TMovimiento] [nvarchar](1) NOT NULL,
	[IDProducto] [int] NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[BodegaEntrada] [int] NULL,
	[FechaEntrada] [date] NULL,
	[BodegaSalida] [int] NULL,
	[FechaSalida] [date] NULL,
	[Cantidad] [int] NOT NULL,
 CONSTRAINT [PK_Inventario] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](60) NOT NULL,
	[FechaCrea] [date] NOT NULL,
	[FechaModi] [date] NULL,
	[Estado] [int] NOT NULL,
	[IDProveedor] [int] NOT NULL,
	[PrecioCompra] [numeric](10, 2) NOT NULL,
	[PrecioVenta] [numeric](10, 2) NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Aprobacion] [int] NULL,
 CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proveedor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Estado] [int] NOT NULL,
	[FechaCrea] [date] NOT NULL,
	[FechaModi] [date] NULL,
	[Aprobacion] [int] NOT NULL,
 CONSTRAINT [PK_Proveedor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tipo_Usuario]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipo_Usuario](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Cargo] [nchar](40) NOT NULL,
 CONSTRAINT [PK_Tipo_Usuario] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Contraseña] [nvarchar](50) NOT NULL,
	[IDTipoUsuario] [int] NOT NULL,
	[FechaCrea] [date] NULL,
	[FechaModi] [date] NULL,
	[Estado] [int] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Venta]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Venta](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDCliente] [int] NOT NULL,
	[MontoTotal] [float] NULL,
	[Aprobacion] [int] NOT NULL,
	[IDProveedor] [int] NULL,
 CONSTRAINT [PK_Venta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Compra]  WITH CHECK ADD  CONSTRAINT [FK_Compra_Clientes] FOREIGN KEY([IDComprador])
REFERENCES [dbo].[Usuario] ([ID])
GO
ALTER TABLE [dbo].[Compra] CHECK CONSTRAINT [FK_Compra_Clientes]
GO
ALTER TABLE [dbo].[DetalleCompraVenta]  WITH CHECK ADD  CONSTRAINT [FK_DetalleCompraVenta_Compra] FOREIGN KEY([IDCompra])
REFERENCES [dbo].[Compra] ([ID])
GO
ALTER TABLE [dbo].[DetalleCompraVenta] CHECK CONSTRAINT [FK_DetalleCompraVenta_Compra]
GO
ALTER TABLE [dbo].[DetalleCompraVenta]  WITH CHECK ADD  CONSTRAINT [FK_DetalleCompraVenta_Factura] FOREIGN KEY([IDFactura])
REFERENCES [dbo].[Factura] ([ID])
GO
ALTER TABLE [dbo].[DetalleCompraVenta] CHECK CONSTRAINT [FK_DetalleCompraVenta_Factura]
GO
ALTER TABLE [dbo].[DetalleCompraVenta]  WITH CHECK ADD  CONSTRAINT [FK_DetalleCompraVenta_Productos] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Productos] ([ID])
GO
ALTER TABLE [dbo].[DetalleCompraVenta] CHECK CONSTRAINT [FK_DetalleCompraVenta_Productos]
GO
ALTER TABLE [dbo].[DetalleCompraVenta]  WITH CHECK ADD  CONSTRAINT [FK_DetalleCompraVenta_Proveedor] FOREIGN KEY([IDProveedor])
REFERENCES [dbo].[Proveedor] ([ID])
GO
ALTER TABLE [dbo].[DetalleCompraVenta] CHECK CONSTRAINT [FK_DetalleCompraVenta_Proveedor]
GO
ALTER TABLE [dbo].[DetalleCompraVenta]  WITH CHECK ADD  CONSTRAINT [FK_DetalleCompraVenta_Venta] FOREIGN KEY([IDVenta])
REFERENCES [dbo].[Venta] ([ID])
GO
ALTER TABLE [dbo].[DetalleCompraVenta] CHECK CONSTRAINT [FK_DetalleCompraVenta_Venta]
GO
ALTER TABLE [dbo].[Inventario]  WITH CHECK ADD  CONSTRAINT [FK_Inventario_Bodega] FOREIGN KEY([BodegaEntrada])
REFERENCES [dbo].[Bodega] ([ID])
GO
ALTER TABLE [dbo].[Inventario] CHECK CONSTRAINT [FK_Inventario_Bodega]
GO
ALTER TABLE [dbo].[Inventario]  WITH CHECK ADD  CONSTRAINT [FK_Inventario_Bodega1] FOREIGN KEY([BodegaSalida])
REFERENCES [dbo].[Bodega] ([ID])
GO
ALTER TABLE [dbo].[Inventario] CHECK CONSTRAINT [FK_Inventario_Bodega1]
GO
ALTER TABLE [dbo].[Inventario]  WITH CHECK ADD  CONSTRAINT [FK_Inventario_Productos] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Productos] ([ID])
GO
ALTER TABLE [dbo].[Inventario] CHECK CONSTRAINT [FK_Inventario_Productos]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Tipo_Usuario] FOREIGN KEY([IDTipoUsuario])
REFERENCES [dbo].[Tipo_Usuario] ([ID])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Tipo_Usuario]
GO
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Clientes] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Clientes] ([ID])
GO
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_Clientes]
GO
/****** Object:  StoredProcedure [dbo].[ActualizarCantidadCompra]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ActualizarCantidadCompra]
@cantidad int,
@id int
as
update Productos set Cantidad = Cantidad + @cantidad where ID = @id;
GO
/****** Object:  StoredProcedure [dbo].[ActualizarCantidadProducto]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[ActualizarCantidadProducto]
@id int,
@Cantidad int
AS
Update Productos set  Cantidad=@Cantidad Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[ActualizarCantidadVenta]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ActualizarCantidadVenta]
@cantidad int,
@id int
as
update Productos set Cantidad = Cantidad - @cantidad where id = @id
GO
/****** Object:  StoredProcedure [dbo].[ActualizarEstadoCompra]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[ActualizarEstadoCompra]
@aprob int,
@id int,
@Proveedor int
as
update Compra set Aprobacion = @aprob, IDProveedor=@Proveedor where id = @id
GO
/****** Object:  StoredProcedure [dbo].[ActualizarEstadoCompraRetirar]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[ActualizarEstadoCompraRetirar]
@aprob int,
@id int
as
update Compra set Aprobacion = @aprob where id = @id
GO
/****** Object:  StoredProcedure [dbo].[ActualizarEstadoVenta]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ActualizarEstadoVenta]
@aprob int,
@id int,
@Proveedor int
as
update Venta set Aprobacion = @aprob, IDProveedor=@Proveedor where id = @id
GO
/****** Object:  StoredProcedure [dbo].[ActualizarEstadoVentaRetirar]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[ActualizarEstadoVentaRetirar]
@aprob int,
@id int
as
update Venta set Aprobacion = @aprob where id = @id
GO
/****** Object:  StoredProcedure [dbo].[AprobarSolicitudCliente]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[AprobarSolicitudCliente]
@id int,
@nombre nchar(50),
@Tipo int,
@FechaModi date
AS
Update Clientes set Nombre= @nombre, Tipo= @tipo, FechaModi=@FechaModi,Aprobacion=1,Estado=1 Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[AsignarFacturaDetalle]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[AsignarFacturaDetalle]
@IDCompra int
AS
Update DetalleCompraVenta set IDFactura=(select top 1 ID from Factura order by ID desc )  Where IDCompra=@IDCompra
GO
/****** Object:  StoredProcedure [dbo].[AsignarFacturaDetalleCliente]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[AsignarFacturaDetalleCliente]
@IDVenta int
AS
Update DetalleCompraVenta set IDFactura=(select top 1 ID from Factura order by ID desc )  Where IDVenta = @IDVenta
GO
/****** Object:  StoredProcedure [dbo].[BusquedaCliente]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[BusquedaCliente]
@tipo int
AS
select id,Nombre,Tipo,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Clientes where Estado = 1  and Tipo = @tipo
GO
/****** Object:  StoredProcedure [dbo].[BusquedaCliente2]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[BusquedaCliente2]
@tipo int
AS
select id,Nombre,Tipo,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Clientes where Estado = 2 and Tipo = @tipo AND Aprobacion= 1
GO
/****** Object:  StoredProcedure [dbo].[BusquedaCliente2Solicitud]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[BusquedaCliente2Solicitud]
@tipo int
AS
select id,Nombre,Tipo,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Clientes where Estado = 2 and Tipo = @tipo AND Aprobacion= 0
GO
/****** Object:  StoredProcedure [dbo].[BusquedaClienteSolicitud]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[BusquedaClienteSolicitud]
@tipo int
AS
select id,Nombre,Tipo,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Clientes where Estado = 0  and Tipo = @tipo
GO
/****** Object:  StoredProcedure [dbo].[BusquedaInventario]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[BusquedaInventario]
@TMovimiento nchar(1),
@IDProducto int,
@IDBodega int
AS
select id, TMovimiento as 'Tipo de Movimiento',IDProducto as 'ID del producto',IDUsuario as 'ID del Usuario',BodegaEntrada as 'Bodega de Entrada', FORMAT (FechaEntrada, 'yyyy-MM-dd') as 'Fecha de Entrada',BodegaSalida as 'Bodega de Salida', FORMAT (FechaSalida, 'yyyy-MM-dd') as 'Fecha de Salida',Cantidad from Inventario where TMovimiento = @TMovimiento and IDProducto =@IDProducto and (BodegaEntrada = @IDBodega OR BodegaSalida =@IDBodega)
GO
/****** Object:  StoredProcedure [dbo].[BusquedaInventarioPrivilegio6]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[BusquedaInventarioPrivilegio6]
@usuario nchar(50)
AS
select id, TMovimiento as 'Tipo de Movimiento',IDProducto as 'ID del producto',IDUsuario as 'ID del Usuario',BodegaEntrada as 'Bodega de Entrada', FORMAT (FechaEntrada, 'yyyy-MM-dd') as 'Fecha de Entrada',BodegaSalida as 'Bodega de Salida', FORMAT (FechaSalida, 'yyyy-MM-dd') as 'Fecha de Salida',Cantidad from Inventario where IDUsuario=@usuario
GO
/****** Object:  StoredProcedure [dbo].[BusquedaInventarioPrivilegio62]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[BusquedaInventarioPrivilegio62]
@TMovimiento nchar(1),
@IDProducto int,
@IDBodega int,
@usuario nchar(50)
AS
select id, TMovimiento as 'Tipo de Movimiento',IDProducto as 'ID del producto',IDUsuario as 'ID del Usuario',BodegaEntrada as 'Bodega de Entrada', FORMAT (FechaEntrada, 'yyyy-MM-dd') as 'Fecha de Entrada',BodegaSalida as 'Bodega de Salida', FORMAT (FechaSalida, 'yyyy-MM-dd') as 'Fecha de Salida',Cantidad from Inventario where TMovimiento = @TMovimiento and IDProducto =@IDProducto and (BodegaEntrada = @IDBodega OR BodegaSalida =@IDBodega) AND IDUsuario=@usuario
GO
/****** Object:  StoredProcedure [dbo].[BusquedaProducto]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[BusquedaProducto]
@id int
AS
select id, Nombre, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad from Productos where IDProveedor=@id and Estado = 1
GO
/****** Object:  StoredProcedure [dbo].[BusquedaProducto2]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[BusquedaProducto2]
@id int
AS
select id, Nombre, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad from Productos where IDProveedor=@id and Estado = 2
GO
/****** Object:  StoredProcedure [dbo].[BusquedaProductoSolicitud]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[BusquedaProductoSolicitud]
@id int
AS
select id, Nombre, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad from Productos where IDProveedor=@id and Estado = 0 AND Aprobacion=0
GO
/****** Object:  StoredProcedure [dbo].[BusquedaProveedor]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROC [dbo].[BusquedaProveedor]
@ID int
AS
select ID,Nombre,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'FechaCrea',FORMAT (FechaModi, 'yyyy-MM-dd') as 'FechaModi' from Proveedor where id=@id
GO
/****** Object:  StoredProcedure [dbo].[BusquedaUsuarios]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[BusquedaUsuarios]
@id int
AS
select id, UserName,Contraseña,IDTipoUsuario, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado from Usuario where Estado = 1 and IDTipoUsuario= @id
GO
/****** Object:  StoredProcedure [dbo].[BusquedaUsuarios2]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[BusquedaUsuarios2]
@Tipo int
AS
select id, UserName,Contraseña,IDTipoUsuario, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado from Usuario where Estado = 2 and IDTipoUsuario= @Tipo
GO
/****** Object:  StoredProcedure [dbo].[CantidadProductoRetornar]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CantidadProductoRetornar]
@id int,
@Retorno int
AS
declare @Cantidad int 
select @Cantidad = Cantidad from Productos where id=@id
Update Productos set  Cantidad=(@Cantidad + @Retorno) Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[DetalleFacturaCompra]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[DetalleFacturaCompra]
@id int
AS
select top 1 DC.ID as 'ID',DC.IDProducto,DC.Cantidad,DC.PrecioUnitario,DC.IDCompra,C.MontoTotal,DC.IDFactura from DetalleCompraVenta DC inner join Compra C on DC.IDCompra = C.ID where C.IDComprador = @id order by DC.ID  DESC
GO
/****** Object:  StoredProcedure [dbo].[DetalleFacturaVenta]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[DetalleFacturaVenta]
@id int
AS
select top 1 DC.ID as 'ID',DC.IDProducto,DC.Cantidad,DC.PrecioUnitario,DC.IDVenta,V.MontoTotal,DC.IDFactura from DetalleCompraVenta DC inner join Venta V on DC.IDVenta = V.ID where V.IDCliente = @id order by DC.ID  DESC
GO
/****** Object:  StoredProcedure [dbo].[EliminarProductoCarrito]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[EliminarProductoCarrito]
@id int
as
Delete DetalleCompraVenta where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[getBodega]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getBodega]
AS
select * from Bodega
GO
/****** Object:  StoredProcedure [dbo].[getCantidadProducto]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getCantidadProducto]
@ID int
AS
select Cantidad from Productos WHERE ID = @ID
GO
/****** Object:  StoredProcedure [dbo].[getCargoUsuario]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getCargoUsuario]
@id int
AS
select id,Cargo from Tipo_Usuario where id = @id
GO
/****** Object:  StoredProcedure [dbo].[getClientes]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[getClientes]
@id int
AS
select id,Nombre,Tipo,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Clientes where ID= @id
GO
/****** Object:  StoredProcedure [dbo].[getClientes2]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getClientes2]
AS
select id,Nombre,Tipo,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Clientes where Estado = 2 AND Aprobacion=1
GO
/****** Object:  StoredProcedure [dbo].[getClientes2Solicitud]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getClientes2Solicitud]
AS
select id,Nombre,Tipo,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Clientes where Estado = 2 AND Aprobacion=0
GO
/****** Object:  StoredProcedure [dbo].[getClientesAlta]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getClientesAlta]
AS
select id,Nombre,Tipo,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Clientes where Estado = 1 AND Aprobacion= 1
GO
/****** Object:  StoredProcedure [dbo].[getClientesAltaSolicitud]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getClientesAltaSolicitud]
AS
select id,Nombre,Tipo,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Clientes where Estado = 0 AND Aprobacion = 0
GO
/****** Object:  StoredProcedure [dbo].[getInventario]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getInventario]
AS
select id, TMovimiento as 'Tipo de Movimiento',IDProducto as 'ID del producto',IDUsuario as 'ID del Usuario',BodegaEntrada as 'Bodega de Entrada', FORMAT (FechaEntrada, 'yyyy-MM-dd') as 'Fecha de Entrada',BodegaSalida as 'Bodega de Salida', FORMAT (FechaSalida, 'yyyy-MM-dd') as 'Fecha de Salida',Cantidad from Inventario
GO
/****** Object:  StoredProcedure [dbo].[getListaComprasAprob]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[getListaComprasAprob]
as
select id,MontoTotal,IDComprador,IDProveedor from Compra where Aprobacion = 0
GO
/****** Object:  StoredProcedure [dbo].[getListaComprasDesAprob]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[getListaComprasDesAprob]
as
select id,MontoTotal,IDComprador,IDProveedor from Compra where Aprobacion = 2
GO
/****** Object:  StoredProcedure [dbo].[getListaVentasAprob]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[getListaVentasAprob]
as
select id,MontoTotal,IDCliente,IDProveedor from Venta where Aprobacion = 0
GO
/****** Object:  StoredProcedure [dbo].[getListaVentasDesAprob]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[getListaVentasDesAprob]
as
select id,MontoTotal,IDCliente,IDProveedor from Venta where Aprobacion = 2
GO
/****** Object:  StoredProcedure [dbo].[getNombreProveedor]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[getNombreProveedor]
@ID int
AS
select id,Nombre from Proveedor where id = @id
GO
/****** Object:  StoredProcedure [dbo].[getProductos]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getProductos]
AS
select id, Nombre, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad from Productos
GO
/****** Object:  StoredProcedure [dbo].[getProductosAlta]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[getProductosAlta]
AS
select id, Nombre, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad from Productos where Estado = 1 AND Aprobacion = 1
GO
/****** Object:  StoredProcedure [dbo].[getProductosAltaSolicitud]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from Usuario

--select * from Productos

--select * from Inventario

--SELECT * FROM clientes

CREATE PROC [dbo].[getProductosAltaSolicitud]
AS
select id, Nombre, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad from Productos where Estado = 0 AND Aprobacion = 0
GO
/****** Object:  StoredProcedure [dbo].[getProductosBaja]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getProductosBaja]
AS
select id, Nombre, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad from Productos where Estado = 2 AND Aprobacion = 1
GO
/****** Object:  StoredProcedure [dbo].[getProductosCompras]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[getProductosCompras]
AS
select ID,Nombre,IDProveedor,PrecioCompra from Productos where Estado = 1
GO
/****** Object:  StoredProcedure [dbo].[getProductosID]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getProductosID]
@id int
AS
select id,Nombre,IDProveedor as 'Proveedor',PrecioCompra as 'Precio de compra',PrecioVenta as 'Precio de venta',Cantidad from Productos where ID=@id
GO
/****** Object:  StoredProcedure [dbo].[getProductosVentas]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getProductosVentas]
AS
select ID,Nombre,IDProveedor,PrecioVenta from Productos where Estado = 1
GO
/****** Object:  StoredProcedure [dbo].[getProveedor]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[getProveedor]
AS
select id,Nombre,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creación',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Proveedor where Estado= 1 AND Aprobacion =1
GO
/****** Object:  StoredProcedure [dbo].[getProveedor2]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[getProveedor2]
AS
select id,Nombre,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creación',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Proveedor where Estado= 2 AND Aprobacion=1
GO
/****** Object:  StoredProcedure [dbo].[getProveedor2Solicitud]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getProveedor2Solicitud]
AS
select id,Nombre,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creación',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Proveedor where Estado= 2 AND Aprobacion=0
GO
/****** Object:  StoredProcedure [dbo].[getProveedorEditar]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getProveedorEditar]
@ID int
AS
select id,Nombre,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creación',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Proveedor where id = @ID
GO
/****** Object:  StoredProcedure [dbo].[getProveedorid]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[getProveedorid]
@ID int
AS
select IDProveedor from Productos where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[getProveedorSolicitud]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from Proveedor

CREATE PROC [dbo].[getProveedorSolicitud]
AS
select id,Nombre,Estado,FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creación',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificación' from Proveedor where Estado= 0 AND Aprobacion =0
GO
/****** Object:  StoredProcedure [dbo].[getSolicitudProductosBaja]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getSolicitudProductosBaja]
AS
select id, Nombre, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad from Productos where Estado = 2 AND Aprobacion = 0
GO
/****** Object:  StoredProcedure [dbo].[getTipoUsuario]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[getTipoUsuario]
AS
select ID,Cargo from Tipo_Usuario
GO
/****** Object:  StoredProcedure [dbo].[getTotalFacturas]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getTotalFacturas]
AS
select ID as 'ID', IDProducto,IDProveedor,Cantidad,PrecioUnitario,IDCompra,IDVenta,IDFactura from DetalleCompraVenta
GO
/****** Object:  StoredProcedure [dbo].[getUsuario]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getUsuario]
@usuario nchar(20)
AS
select id,IDTipoUsuario from Usuario WHERE UserName = @usuario
GO
/****** Object:  StoredProcedure [dbo].[getUsuarioCliente]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getUsuarioCliente]
@usuario nchar(50)
AS
select id from Clientes WHERE Nombre = @usuario
GO
/****** Object:  StoredProcedure [dbo].[getUsuarios]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[getUsuarios]
@id int
AS
select id, UserName,Contraseña,IDTipoUsuario, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado from Usuario where ID=@id
GO
/****** Object:  StoredProcedure [dbo].[getUsuariosAlta]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getUsuariosAlta]
AS
select id, UserName,Contraseña,IDTipoUsuario, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado from Usuario where Estado = 1
GO
/****** Object:  StoredProcedure [dbo].[getUsuariosBaja]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[getUsuariosBaja]
AS
select id, UserName,Contraseña,IDTipoUsuario, FORMAT (FechaCrea, 'yyyy-MM-dd') as 'Fecha Creacion',FORMAT (FechaModi, 'yyyy-MM-dd') as 'Fecha Modificacion',Estado from Usuario where Estado = 2
GO
/****** Object:  StoredProcedure [dbo].[insertarBodega]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[insertarBodega]
@Nombre nchar(50),
@Direccion nchar(80),
@FechaCreacion date
AS
INSERT INTO Bodega (Nombre,Direccion,FechaCrea) VALUES (@Nombre,@Direccion,@FechaCreacion)
GO
/****** Object:  StoredProcedure [dbo].[insertarCliente]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[insertarCliente]
@Nombre nchar(50),
@Tipo int,
@FechaCreacion date,
@Aprobacion int,
@Estado int,
@Contraseña nchar(50)
AS
INSERT INTO Clientes (Nombre,Tipo,FechaCrea,Aprobacion,Estado,Contraseña) VALUES (@Nombre,@Tipo,@FechaCreacion,@Aprobacion,@Estado,@Contraseña)
GO
/****** Object:  StoredProcedure [dbo].[InsertarCompra]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[InsertarCompra]
@Aprobacion int,
@IDComprador int
AS
INSERT INTO Compra(Aprobacion,IDComprador) VALUES (@Aprobacion,@IDComprador)
GO
/****** Object:  StoredProcedure [dbo].[InsertarDetalleCompra]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[InsertarDetalleCompra]
@idProducto int,
@IDProveedor int,
@Cantidad int,
@PrecioUnitario float,
@IDCompra int
AS
INSERT INTO DetalleCompraVenta(IDProducto,IDProveedor,Cantidad,PrecioUnitario,IDCompra) VALUES (@idProducto,@IDProveedor,@Cantidad,@PrecioUnitario,@IDCompra)
GO
/****** Object:  StoredProcedure [dbo].[InsertarDetalleVenta]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[InsertarDetalleVenta]
@idProducto int,
@IDProveedor int,
@Cantidad int,
@PrecioUnitario float,
@IDventa int
AS
INSERT INTO DetalleCompraVenta(IDProducto,IDProveedor,Cantidad,PrecioUnitario,IDVenta) VALUES (@idProducto,@IDProveedor,@Cantidad,@PrecioUnitario,@IDventa)
GO
/****** Object:  StoredProcedure [dbo].[InsertarMontoTotal]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[InsertarMontoTotal]
@monto decimal(10,2),
@id int
as
update compra set MontoTotal = @monto where id = @id
GO
/****** Object:  StoredProcedure [dbo].[InsertarMontoTotal2]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[InsertarMontoTotal2]
@monto decimal(10,2),
@id int
as
update Venta set MontoTotal = @monto where id = @id
GO
/****** Object:  StoredProcedure [dbo].[insertarMovimiento]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[insertarMovimiento]
@TMovimiento nchar(1),
@IDProducto int,
@IDUsuario int,
@BodegaEntrada int,
@FechaEntrada date,
@Cantidad int
AS
INSERT INTO Inventario (TMovimiento,IDProducto,IDUsuario,BodegaEntrada,FechaEntrada,Cantidad) VALUES (@TMovimiento,@IDProducto,@IDUsuario,@BodegaEntrada,@FechaEntrada,@Cantidad)
GO
/****** Object:  StoredProcedure [dbo].[insertarMovimiento2]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[insertarMovimiento2]
@TMovimiento nchar(1),
@IDProducto int,
@IDUsuario int,
@BodegaSalida int,
@FechaSalida date,
@Cantidad int
AS
INSERT INTO Inventario (TMovimiento,IDProducto,IDUsuario,BodegaSalida,FechaSalida,Cantidad) VALUES (@TMovimiento,@IDProducto,@IDUsuario,@BodegaSalida,@FechaSalida,@Cantidad)
GO
/****** Object:  StoredProcedure [dbo].[insertarProducto]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[insertarProducto]
@Nombre nchar(60),
@FechaCrea date,
@Estado int,
@IDProveedor int,
@PrecioCompra numeric(10, 2),
@PrecioVenta numeric(10, 2),
@Cantidad int,
@Aprobacion int
AS
INSERT INTO Productos (Nombre,FechaCrea,Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad,Aprobacion) VALUES (@Nombre,@FechaCrea,@Estado,@IDProveedor,@PrecioCompra,@PrecioVenta,@Cantidad,@Aprobacion)
GO
/****** Object:  StoredProcedure [dbo].[insertarProveedor]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[insertarProveedor]
@Nombre nchar(50),
@Estado int,
@FechaCreacion date,
@Aprobacion int
AS
INSERT INTO Proveedor (Nombre,Estado,FechaCrea,Aprobacion) VALUES (@Nombre,@Estado,@FechaCreacion,@Aprobacion)
GO
/****** Object:  StoredProcedure [dbo].[insertarUsuario]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[insertarUsuario]
@Nombre nchar(20),
@Contraseña nchar(20),
@IDTipoUsuario int,
@FechaCrea date,
@Estado int
AS
INSERT INTO Usuario (UserName,Contraseña,IDTipoUsuario,FechaCrea,Estado) VALUES (@Nombre,@Contraseña,@IDTipoUsuario,@FechaCrea,@Estado)
GO
/****** Object:  StoredProcedure [dbo].[InsertarVenta]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[InsertarVenta]
@Aprobacion int,
@IDCliente int
AS
INSERT INTO Venta(IDCliente,Aprobacion) VALUES (@IDCliente,@Aprobacion)
GO
/****** Object:  StoredProcedure [dbo].[LoginUser]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LoginUser]
@usuario nchar(20),
@contraseña nchar(20)
AS
SELECT * fROM Usuario WHERE UserName=@usuario AND Contraseña=@contraseña
GO
/****** Object:  StoredProcedure [dbo].[LoginUserCliente]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[LoginUserCliente]
@usuario nchar(20),
@contraseña nchar(20)
AS
SELECT * fROM Clientes WHERE Nombre=@usuario AND Contraseña=@contraseña
GO
/****** Object:  StoredProcedure [dbo].[modificarCliente]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[modificarCliente]
@id int,
@nombre nchar(50),
@Tipo int,
@FechaModi date
AS
Update Clientes set Nombre= @nombre, Tipo= @tipo, FechaModi=@FechaModi Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[modificarClienteEstado]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[modificarClienteEstado]
@id int,
@Estado int,
@modi date
AS
Update Clientes set Estado=@Estado,FechaModi=@modi Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[modificarProducto]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[modificarProducto]
@id int,
@Nombre nchar(60),
@IDProveedor int,
@PrecioCompra numeric(10, 2),
@PrecioVenta numeric(10, 2),
@Cantidad int,
@FechaModi date
AS
Update Productos set Nombre= @nombre, IDProveedor= @IDProveedor, PrecioCompra= @PrecioCompra, PrecioVenta=@PrecioVenta, Cantidad=@Cantidad,FechaModi=@FechaModi Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[modificarProductoEstado]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[modificarProductoEstado]
@id int,
@Estado int,
@Modi date
AS
Update Productos set Estado=@Estado,FechaModi=@modi Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[modificarProveedor]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[modificarProveedor]
@id int,
@Estado int,
@modi date
AS
Update Proveedor set Estado=@Estado,FechaModi=@modi Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[modificarProveedorTotal]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[modificarProveedorTotal]
@id int,
@nombre nchar(50),
@modi date
AS
Update Proveedor set nombre=@nombre,FechaModi=@modi Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[modificarProveedorTotalSolicitud]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[modificarProveedorTotalSolicitud]
@id int,
@nombre nchar(50),
@modi date
AS
Update Proveedor set nombre=@nombre,FechaModi=@modi,Estado=1,Aprobacion=1 Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[modificarUsuario]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[modificarUsuario]
@id int,
@user nchar(20),
@Contraseña nchar(20),
@IDUsuario int,
@Modi date
AS
Update Usuario set UserName=@user,Contraseña=@Contraseña,IDTipoUsuario=@IDUsuario, FechaModi=@Modi Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[modificarUsuarioEstado]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[modificarUsuarioEstado]
@id int,
@Estado int,
@Modi date
AS
Update Usuario set Estado=@Estado,FechaModi=@modi Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[ObtenerTotal]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[ObtenerTotal]
as
Select Sum(DC.Cantidad*DC.PrecioUnitario) as 'Precio' from DetalleCompraVenta DC where (select IDENT_CURRENT('compra') as 'Ultimo') = DC.IDCompra
GO
/****** Object:  StoredProcedure [dbo].[ObtenerTotal2]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ObtenerTotal2]
as
Select Sum(DC.Cantidad*DC.PrecioUnitario) as 'Precio' from DetalleCompraVenta DC where (select IDENT_CURRENT('Venta') as 'Ultimo') = DC.IDVenta
GO
/****** Object:  StoredProcedure [dbo].[SolicitudAprobadaProducto]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[SolicitudAprobadaProducto]
@id int,
@Nombre nchar(60),
@IDProveedor int,
@PrecioCompra numeric(10, 2),
@PrecioVenta numeric(10, 2),
@Cantidad int,
@FechaModi date
AS
Update Productos set Nombre= @nombre, IDProveedor= @IDProveedor, PrecioCompra= @PrecioCompra, PrecioVenta=@PrecioVenta, Cantidad=@Cantidad,FechaModi=@FechaModi,Aprobacion=1,Estado=1 Where ID = @id
GO
/****** Object:  StoredProcedure [dbo].[UltimoIDCompra]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[UltimoIDCompra]
as
select IDENT_CURRENT('compra') as 'Ultimo'
GO
/****** Object:  StoredProcedure [dbo].[UltimoIDVenta]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[UltimoIDVenta]
as
select IDENT_CURRENT('Venta') as 'Ultimo'
GO
/****** Object:  StoredProcedure [dbo].[VerCarrito]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[VerCarrito]
as
Select DC.ID,P.Nombre,DC.Cantidad*DC.PrecioUnitario as 'Precio',DC.Cantidad,P.IDProveedor as 'IDProveedor' from Compra C INNER JOIN DetalleCompraVenta DC ON (C.ID=DC.IDCompra) INNER JOIN Productos P ON (DC.IDProducto=P.ID) where (select IDENT_CURRENT('compra') as 'Ultimo') = DC.IDCompra
GO
/****** Object:  StoredProcedure [dbo].[VerCarrito2]    Script Date: 6/5/2020 08:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[VerCarrito2]
as
Select DC.ID,P.ID as 'idProducto',P.Nombre,DC.Cantidad*DC.PrecioUnitario as 'Precio',DC.Cantidad from Venta C INNER JOIN DetalleCompraVenta DC ON (C.ID=DC.IDVenta) INNER JOIN Productos P ON (DC.IDProducto=P.ID) where (select IDENT_CURRENT('Venta') as 'Ultimo') = DC.IDVenta
GO
USE [master]
GO
ALTER DATABASE [Fenix_Alimentos] SET  READ_WRITE 
GO
Use Fenix_Alimentos
go
DBCC CHECKIDENT (Usuario, RESEED,1)
DBCC CHECKIDENT (Tipo_Usuario, RESEED,1)
DBCC CHECKIDENT (Bodega, RESEED,1)
DBCC CHECKIDENT (Clientes, RESEED,1)
DBCC CHECKIDENT (Usuario, RESEED,1)
DBCC CHECKIDENT (Compra, RESEED,1)
DBCC CHECKIDENT (DetalleCompraVenta, RESEED,1)
DBCC CHECKIDENT (Factura, RESEED,1)
DBCC CHECKIDENT (Inventario, RESEED,1)
DBCC CHECKIDENT (Productos, RESEED,1)
DBCC CHECKIDENT (Proveedor, RESEED,1)
DBCC CHECKIDENT (Venta, RESEED,1)
go
insert into Tipo_Usuario (Cargo) values ('Administradores')
insert into Tipo_Usuario (Cargo) values ('Jefes de inventario')
insert into Tipo_Usuario (Cargo) values ('Jefes de compras')
insert into Tipo_Usuario (Cargo) values ('Jefes de relaciones Exteriores')
insert into Tipo_Usuario (Cargo) values ('Jefes de Facturación')
insert into Tipo_Usuario (Cargo) values ('Usuarios de inventario')
insert into Tipo_Usuario (Cargo) values ('Usuarios de compras')
insert into Tipo_Usuario (Cargo) values ('Usuarios de relaciones exteriores')
insert into Tipo_Usuario (Cargo) values ('Usuarios de facturación')
go
insert into Usuario (UserName,Contraseña,IDTipoUsuario,FechaCrea,Estado) values ('Admin','123','1','2020-05-07',1)
insert into Usuario (UserName,Contraseña,IDTipoUsuario,FechaCrea,Estado) values ('JI','123','2','2020-05-07',1)
insert into Usuario (UserName,Contraseña,IDTipoUsuario,FechaCrea,Estado) values ('JC','123','3','2020-05-07',1)
insert into Usuario (UserName,Contraseña,IDTipoUsuario,FechaCrea,Estado) values ('JRE','123','4','2020-05-07',1)
insert into Usuario (UserName,Contraseña,IDTipoUsuario,FechaCrea,Estado) values ('JF','123','5','2020-05-07',1)
insert into Usuario (UserName,Contraseña,IDTipoUsuario,FechaCrea,Estado) values ('UI','123','6','2020-05-07',1)
insert into Usuario (UserName,Contraseña,IDTipoUsuario,FechaCrea,Estado) values ('UC','123','7','2020-05-07',1)
insert into Usuario (UserName,Contraseña,IDTipoUsuario,FechaCrea,Estado) values ('URE','123','8','2020-05-07',1)
insert into Usuario (UserName,Contraseña,IDTipoUsuario,FechaCrea,Estado) values ('UF','123','9','2020-05-07',1)
go
insert into Bodega(Nombre,Direccion,FechaCrea) values ('Bodegas Nesman','Franklin Villanueva 3826','2020-05-07')
insert into Bodega(Nombre,Direccion,FechaCrea) values ('Bodegas Tapaus','Calle Agustin Lara No. 69-B','2020-05-07')
insert into Bodega(Nombre,Direccion,FechaCrea) values ('Bodegas Murviedro','Calle Matamoros No.280','2020-05-07')
go
insert into Clientes(Nombre,Tipo,FechaCrea,Aprobacion,Estado,Contraseña) values ('Hector Ortuño',1,'2020-05-07',1,1,'HO001')
insert into Clientes(Nombre,Tipo,FechaCrea,Aprobacion,Estado,Contraseña) values ('Naima Gómez',2,'2020-05-07',1,1,'NG001')
insert into Clientes(Nombre,Tipo,FechaCrea,Aprobacion,Estado,Contraseña) values ('Julia Moyano',1,'2020-05-07',1,1,'JM001')
insert into Clientes(Nombre,Tipo,FechaCrea,Aprobacion,Estado,Contraseña) values ('Pedro Menendez',1,'2020-05-07',0,0,'PM001')
go
insert into Proveedor(Nombre,Estado,FechaCrea,Aprobacion) values ('Unoaceit',1,'2020-05-07',1)
insert into Proveedor(Nombre,Estado,FechaCrea,Aprobacion) values ('Agrolarrosa',1,'2020-05-07',1)
insert into Proveedor(Nombre,Estado,FechaCrea,Aprobacion) values ('Dialpi',1,'2020-05-07',1)
insert into Proveedor(Nombre,Estado,FechaCrea,Aprobacion) values ('Comercial GoodFood',0,'2020-05-07',0)
go
insert into Productos(Nombre,FechaCrea,Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad,Aprobacion) values('Aceite Borges Oliva Extravirg 125 Ml','2020-05-07',1,1,1.45,1.89,12,1)
insert into Productos(Nombre,FechaCrea,Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad,Aprobacion) values('Aderezo Cesar Con Queso Asiago 12 Onz','2020-05-07',1,3,4.60,6.12,9,1)
insert into Productos(Nombre,FechaCrea,Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad,Aprobacion) values('Arroz Alessi Risoto A La Milanese 227 Grs','2020-05-07',1,2,4.12,5.24,43,1)
insert into Productos(Nombre,FechaCrea,Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad,Aprobacion) values('ChampiÑOn Bandeja 8 Onz','2020-05-07',1,3,4.15,4.40,21,1)
insert into Productos(Nombre,FechaCrea,Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad,Aprobacion) values('Uva Roja Sin Semilla Flame Lb','2020-05-07',1,2,2.00,2.40,102,1)
insert into Productos(Nombre,FechaCrea,Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad,Aprobacion) values('Frijol De Soya Congelado Edame 400 Grs','2020-05-07',1,2,3.90,4.35,20,1)
insert into Productos(Nombre,FechaCrea,Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad,Aprobacion) values('Leche Semidescremada Galon','2020-05-07',0,3,4.21,5.15,38,0)
insert into Productos(Nombre,FechaCrea,Estado,IDProveedor,PrecioCompra,PrecioVenta,Cantidad,Aprobacion) values('Queso Alemette Untable Con Hirbas 150 Grs','2020-05-07',0,3,2.87,3.64,20,0)